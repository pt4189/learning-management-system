DemoCloud::Application.routes.draw do 
  
  mount Forem::Engine, :at => "/cloudforum" 
  resources :messages do
    get 'subject', :on => :member
    get 'forward', :on => :member
    get 'reply', :on => :member

  end


  resources :videos

  resources :admin_grades

  get "messages/index"
  match '/admin_grades', :to =>'admin_grades#index'
  resources :events

  get "password_resets/new"
  match '/calender_asses', :to =>'assignments#calender_asses'
  match '/calender_asses/filter', :to =>'assignments#filter'
  
  match '/change_password', :controller => 'users', :action => 'change_password'
   
   resources :calenders, :only => [:index] do
     get 'view_assignment', :on => :member
   end
   
   match '/courses/:id/subject', :to => "courses#subject"
   resources :courses do
     match '/check_assignments', :to =>'course_classes#check_assignments'
     resources :course_classes do
       
       
     end
     resources :student_infos do
          match '/check_student', :to =>'student_infos#check_student', on: :collection
          get 'student', :on => :member
    end
     resources :assignments do
       match 'student', :to => "assignments#student"
       match 'student/:id/submit_assignment', :to => "assignments#submit_assignment"
       match '/questions/:id/correct_answer', :to => "questions#correct_answer"
       resources :questions do
         match '/sub_questions/:id/correct_answer', :to => "sub_questions#correct_answer"
        resources :sub_questions
     
       end
     end 
     
   end

    resources :users do
      member do
        post :update_meta
      end
      collection do 
        get :course_index
        post :update_iid
        post :create_student_info
      end
    end   
    resources :institutions
    resources :sessions, :only => [:new,:create,:destroy,:edit]
    root :to => 'sessions#new'
    match 'dashboard' => 'users#dashboard', :as => 'user_root'
    match '/signup', :to =>'users#new'
    match '/signin', :to =>'sessions#new'
    match  '/users/:id/edit',  :to => "users#edit" 
    match '/signout', :to =>'sessions#destroy'
    match '/user/signout', :to =>'sessions#user_signout', :via => :post
   resources :password_resets
    match '/institutions/:id/delete', :to => "institutions#destroy"
    match '/edu_classes/check_terms', :to => "edu_classes#check_terms"
    match '/institutions/check_terms', :to => "institutions#check_terms"
    match '/edu_class/:id/status', :to => "edu_classes#toggled_status"
    match '/edu_classes/:id/delete', :to => "edu_classes#destroy"
    
    match '/units/:id/delete', :to => "units#destroy"
    match '/chapters/:id/delete', :to => "chapters#destroy"
    match '/topics/:id/delete', :to => "topics#destroy"
    match '/sub_topics/:id/delete', :to => "sub_topics#destroy"
    match '/questions/:id/delete', :to => "questions#destroy"
    match '/sub_questions/:id/delete', :to => "sub_questions#destroy"
    match '/colors/:id/delete', :to => "colors#destroy"
    match '/users/:id/delete', :to => "users#destroy"
    match '/users/:id/student', :to => "users#student"
     match '/users/:id/student/add', :to => "users#add"
     match '/users/:id/student/:id2/edit', :to => "users#edit_student", :as => :edit_student
     match '/users/:id/student/index', :to => "users#index"
     match '/users/:id/teacher', :to => "users#teacher"
     match '/users/:id/teacher/add_teacher', :to => "users#add_teacher"
     match '/users/:id/teacher/add_teacher/:id2/edit_teacher', :to => "users#edit_teacher", as: :edit_teacher
     match '/student_class/check_student_class', :to => "student_classes#check_student_class"
     match '/student_class/toggled_status', :to => "student_classes#toggled_status"
     match '/student_class', :to => "student_classes#index"
     match '/users/validations/check_email', :to=>"users#check_email"
     match '/users/validations/check_username', :to=>"users#check_username"
     match '/users/:subject_id/student_info/new/', :to=>"users#new_student_info", as: :course_new_student_info

     match '/password_resets/validations/check_pass_reset', :to=>"password_resets#check_pass_reset"
     
     match '/users/dashboard/confirm_destroy', :to => "users#confirm_destroy"
     
      match '/users/dashboard/confirm_setting', :to => "users#confirm_setting"
       match '/users/dashboard/setting', :to => "users#setting"
     
   match '/edu_classes/:edu_class_id/assignments/:id/delete', :to => "assignments#destroy"
   match '/edu_classes/:edu_class_id/assignments/:assignment_id/questions/:id/delete', :to => "questions#destroy"
   match '/edu_classes/:edu_class_id/assignments/:assignment_id/questions/:question_id/sub_questions/:id/delete', :to => "sub_questions#destroy"
     #add institute
      match '/users/:id/institute', :to => "users#institute"
      match '/users/:id/institute/add_institute', :to => "users#add_institute"
      match '/users/:id/institute/add_institute/:id2/edit_institute', :to => "users#edit_institute", as: :edit_institute
     resources :edu_classes do
       get :students, :on => :member
       get :teachers, :on => :member
     
      end
   
   match '/assignments/check_duedate', :to => "assignments#check_duedate"
   match '/assignments/check_unit', :to => "assignments#check_unit"
   match '/assignments/check_chapter', :to => "assignments#check_chapter"
   match '/assignments/check_topic', :to => "assignments#check_topic"
   match '/assignments/check_subtopic', :to => "assignments#check_subtopic"
   match '/assignments/pdf_upload', :to => "assignments#pdf_upload"
   
   match '/student/:id/check_assignment/:assignment_id', :to => "student_infos#check_assignment"
   
   match '/student/:id/result/:assignment_id', :to => "student_infos#result", :via => :post
   
   match '/staffs/check_class', :to => "staffs#check_class"
   match '/staffs/toggled_status', :to => "staffs#toggled_status"
   resources :staffs
   
   match '/subjects/check_subject', :to=>"subjects#check_subject"
   match '/subjects/:id/delete', :to => "subjects#destroy"
   resources :subjects do
    member do
      post :check_terms
    end
   end
    match '/units/check_subject', :to => "units#check_unit"
    match '/units/:id/delete', :to => "units#destroy"

   resources :units 
   match '/chapters/check_chapter', :to => "chapters#check_chapter"
   match '/chapters/:id/delete', :to => "chapters#destroy"
   resources :chapters
   match '/topics/check_topic', :to => "topics#check_topic"
    match '/topics/:id/delete', :to => "topics#destroy"
   resources :topics
   match '/sub_topics/check_subtopic', :to => "sub_topics#check_subtopic"
   match '/sub_topics/:id/delete', :to => "sub_topics#destroy"
   resources :sub_topics
   
   resources :student_assignments do
     resources :assign_ques
     resources :solve_pdf_assignments
   end
   
  
                                     
   
   resources :colors
    post '/class_settings/update', :to => "class_settings#update"
    post '/class_settings/update_color', :to => "class_settings#update_color"
    post '/class_settings/update_remainder', :to => "class_settings#update_remainder"

    namespace :admin do
    resources :people, only:[] do
      collection do
        match ':type', action: "index", as: 'index'
        match '/student_show/:id', action: "student_show", as: 'student_show'
      end
      
    end
  end
   
  end