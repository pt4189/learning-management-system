namespace :existing do
  desc "Create forum for exisiting courses"
  task :create_forum => :environment do
    Subject.create_forum_for_existing_records
  end
end