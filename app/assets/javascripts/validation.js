jQuery(document).ready(function() {
  jQuery("#new_user").validate({
	errorElement:'div',
	rules: {
		"user[first_name]":{
					                  required: true
											 },
	  "user[last_name]":{
					                  required: true
					      },
	  "user[username]":{
							  required: true,
							minlength:4,
							remote:"/users/validations/check_username"
						},
	  "user[email]":{
								required:true,
								email:true,
								remote:"/users/validations/check_email"
						},
	  "user[password]":{
						required:true,
						minlength:6
					},
	  "user[password_confirmation]":{
										required:true,
										equalTo: "#user_password"
									}
						
		},
	messages: {
		"user[first_name]":{
		                   	required:  "Please enter the first name"
                    		},
  	"user[last_name]":{
		                	required:  "Please enter the last name"
											},
	 "user[username]":{
							required: "Please enter username",
							minlength:jQuery.format("do not enter less than 4 characters"),
							remote:"Username already exists"
					 },
	 "user[email]":{
						required: "Please enter email address",
						email: "Please enter valid email id",
						remote:"email id already exists"
				 },
	 "user[password]":{
						required: "Please enter password",
						minlength:jQuery.format("do not enter less than 6 characters")
	              },
	  			"user[password_confirmation]":{
													required:"Please enter confirmation password",
													equalTo: "Password does not match"
												}
				
																
		}
		
		
	});
	
	
	
	
	
		
		jQuery("#new_question").validate({
			errorElement:'div',
			rules: {
				"question[question]" : { required: true },
				"question[correct_answer]" : { required: true }

				},
			messages: {
				"question[question]":{ required:  "Please enter the question" },
		        "question[correct_answer]" : { required:  "Please enter the correct answer" }


				}

			});
			
			
			jQuery("#password_reset").validate({
					errorElement:'div',
					rules: {
					"email":{
										required: true,
										email: true,
										remote:"/password_resets/validations/check_pass_reset"
								}
					},
					messages: {
						"email":{
								required: "Please enter email address",
								email: "Please enter valid email id",
								remote:"email id already exists"
						 }

						}

					});
			
			
			jQuery("#new_sub_question").validate({
				errorElement:'div',
				rules: {
					"sub_question[subquestion]" : { required: true },
					"sub_question[correct_answer]" : { required: true }

					},
				messages: {
					"sub_question[subquestion]":{ required:  "Please enter the question" },
			        "sub_question[correct_answer]" : { required:  "Please enter the correct answer" }


					}

				});
	          
		
	});
	
	


	jQuery(function() {
	  jQuery("#student th a, #student .pagination a").live("click", function() {
	    jQuery.getScript(this.href);
	    return false;
	  });
	 jQuery("#staffs th a, #staffs .pagination a").live("click", function() {
	    jQuery.getScript(this.href);
	    return false;
	  });
	 
	});	


function remove_fields(link) {
  alert($(link).up(".fields"));
  $(link).previous("input[type=hidden]").value = "1";
  $(link).up(".fields").hide();
}

function add_fields(link, association, content) {
  var new_id = new Date().getTime();
  var regexp = new RegExp("new_" + association, "g")
  $(link).up().insert({
    before: content.replace(regexp, new_id)
  });
}





jQuery(document).ready(function() {

  jQuery(".edit_user").validate({
	errorElement:'div',
	rules: {
		
		"user[first_name]":{ required: true },
		
	  "user[last_name]":{ required: true },
	
	  "user[email]":{ required:true, email:true },
						
		},
	messages: {
		"user[first_name]":{ required:  "Please enter the first name" },
		
  	"user[last_name]":{ required:  "Please enter the last name" },
	
	 "user[email]":{ required: "Please enter email address", email: "Please enter valid email id" }
				
		}
		
	});

 });


