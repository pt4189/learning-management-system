defaultWidth	= 307; //pixels
transition		= 500; //millisecond

function resetMargin(width) {

	divLeftMargin	= 0;

	jQuery('.additional-block').each(function() {
		
		thisLeftMargin	= divLeftMargin + 'px';
		
		jQuery(this).css('margin-left', thisLeftMargin);
		
		divLeftMargin	= divLeftMargin + width;
		
	});
}

function styleSettings(){
	jQuery("#MB_frame").css("background", "transparent");
	jQuery("#MB_close").css("opacity", "0");
	jQuery("#MB_window").css("top", "100px");
	jQuery("#modal-box").css("overflow", "hidden");
	resetMargin(defaultWidth);



	jQuery('.remainder_check').change(function(event){
		var class_id = jQuery(this).attr('data-class-id');
		var remainder_for = jQuery(this).attr('data-remainder-for');
		var remainder_type = jQuery(this).attr('data-remainder-type');
		var remainder_type_underscore = jQuery(this).attr('data-remainder-type-underscore');
		var content_checkbox = class_id + "_remainder_for_" + remainder_for + "_" + remainder_type_underscore;
		jQuery('#'+content_checkbox).toggleClass("greeny");
		jQuery.post('/class_settings/update_remainder', 
		{	
			class_setting_id:class_id,
			value:$(this).checked,
			remainder_for:remainder_for,
			remainder_type:remainder_type
		});
	});

	jQuery('.automatic_remainder_checkbox').change(function(event){
		var class_id = jQuery(this).attr('data-class-id');
		jQuery('.sub_remainders').toggleClass('hidden');
		jQuery.post('/class_settings/update',{class_setting_id:class_id,value:$(this).checked, remainder_for:'automatic_remainder'});
	});

	jQuery('.lms-class-color').change(function(event){
		var class_id = jQuery(this).attr('data-class-id');
		var isStudent = jQuery(this).attr('data-is-student');
		if(isStudent == 'true')
		{
			jQuery.post('/class_settings/update',{class_setting_id:class_id,value:$(this).value, remainder_for:'color'});
		}
		else
		{
			jQuery.post('/class_settings/update_color',{class_setting_id:class_id,value:$(this).value, remainder_for:'color'},
				function(data)
				{
					if(data != "")
					{
						data = data.split(",")
						jQuery(".class"+data[0]+"-color-box").css("background-color", data[1]);
						jQuery('.select-'+data[0]).val(data[1]);
					}

				});
		}
		jQuery(".class"+class_id+"-color-box").css("background-color", $(this).value);
	});

	jQuery("#change_password_link").click(function(){
		if(jQuery('#change_password').parent().height() != 220)
		{
			document.getElementById('change_password_form').reset();
			jQuery('.status_text').html("");
			jQuery('#change_password').css("display", "block");
			jQuery('#change_password').parent().height("220px")
		}
		else
		{
			jQuery('#change_password').css("display", "none");
			jQuery('#change_password').parent().height("auto")
		}
	});

	jQuery('#change_password_form').submit(function(e){
		jQuery.post('/change_password', jQuery(this).serialize(), function(e){
			jQuery('#change_password p.status_text').html(e);
			document.getElementById('change_password_form').reset();
		});

		e.preventDefault();
		return false;
	});

	jQuery('#user_settings_language').change(function(e){
		jQuery.post(jQuery(this).attr('data-url'),{'user':{'language':jQuery(this).val()}});
	});

	jQuery(".accordion").accordion({collapsible: true, active: false});

	jQuery('.edit').each(function(){
		var toEditable = jQuery(this);
		var url = toEditable.attr("data-url");
		var name = toEditable.attr("name");
		toEditable.editable(url, { 
			name : name,
			indicator : 'Saving...',
		});
	});

	jQuery("#profile_info h3").click(function(){
		jQuery(".class_holder").toggleClass("hidden");
		jQuery("#profile_info").toggleClass("no_border");
		jQuery("#profile_info h3").toggleClass("profile_info_h3_border_bottom");
	})

	jQuery('#settings_container .binder .menu a').each(function() {
		thisHref = jQuery(this).attr('href');
		if(jQuery(thisHref).length > 0) {
			jQuery(this).addClass('has-child');
		}
	});


	jQuery('#settings_container .binder .menu li[id!="accordion"] a').bind('click', function(event) {

		event.preventDefault();

		selectedDiv			= jQuery(this).attr('href');
		selectedMargin		= jQuery(selectedDiv).css('margin-left');
		selectedParent		= jQuery(this).parents('.additional-block');
		sliderMargin		= jQuery('.slider').css('margin-left');
		slidingMargin		= (parseInt(sliderMargin) - defaultWidth) + 'px';


		if(selectedMargin.length > 0) {

			jQuery(selectedDiv).children('.header').children(".header_container").prepend('<span class="back"></span>').bind('click', function () {

				selectedParent	= jQuery(this).parents('.additional-block');
				sliderMargin	= - (parseInt(selectedParent.css('margin-left')) - defaultWidth) + 'px';
				jQuery('.slider').animate({marginLeft: sliderMargin}, transition);

			});
			
			if((parseInt(selectedMargin) - defaultWidth) >= defaultWidth) {

				selectedParent.after(jQuery(selectedDiv));

				resetMargin(defaultWidth);

				jQuery('#settings_container .binder .slider').animate({marginLeft: slidingMargin}, transition);

			} else {

				jQuery('#settings_container .binder .slider').animate({marginLeft: slidingMargin}, transition);

			}
		}
	});
}