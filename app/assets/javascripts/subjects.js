function initializeSubjectsPopup()
{
	jQuery('#MB_window').css('top', '50px');
	initializeSearchAndAdd('users/course_index.json/?type=t', 'teachers')
	initializeSearchAndAdd('users/course_index.json/?type=s', 'students')
}

function initializeSearchAndAdd(url, type)
{
	jQuery.getJSON(url, function(data) {
		jQuery('#subject_'+type).autocomplete({
			source: data,
			focus: function( event, ui ) {
				jQuery( "#subject_"+ type ).val( ui.item.label );
				return false;
			},
			select: function( event, ui ) {
				if(ui.item)
				{
					var entity_id = ui.item.value;
					var entity_name = ui.item.label;
					if(jQuery.inArray(entity_id.toString(), jQuery("#"+type).val().split(",")) == -1)
					{
						var values = jQuery("#"+ type).val();
						var arr = values == "" ? [] : values.split(",")
						arr.push(ui.item.value)
						jQuery("#"+ type).val(arr.join(","));
						jQuery(".show_"+ type +" ul").prepend("<li data-id='"+entity_id+"'>"+entity_name+"  <a class='remove'>x</a></li>")
					}
					jQuery( "#subject_"+type ).val("");
					return false;
				}
			}

		});
	});

	jQuery(".show_"+ type +" li a.remove").live('click', function(e){
		if(jQuery(e.target).data('oneclicked')!='yes')
		{
			var conatiner = jQuery(e.target).parent()
			var entity_id = conatiner.attr('data-id');

			var arr = jQuery("#"+type).val().split(",")
			jQuery("#"+type).val(arr.without(entity_id).join(","));

			conatiner.remove();
			e.stopPropagation();
		}
		jQuery(e.target).data('oneclicked','yes');
	});

}