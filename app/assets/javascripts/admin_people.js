function afterStudentPopupLoaded(){
			jQuery("#MB_window").css("top", "120px");
			jQuery("#MB_frame").addClass("student_show_popup_frame");
			jQuery('.previous_student').click(function(e){
				var url = jQuery(this).attr('data-link');
				jQuery('#modal-box').fadeOut('slow',function(){
					jQuery.get(url,function(data){
						jQuery('#modal-box').html(data);
						jQuery('#modal-box').fadeIn('slow');
						afterStudentPopupLoaded();
					});
				});
			});
			jQuery('.next_student').click(function(e){
				var url = jQuery(this).attr('data-link');
				jQuery('#modal-box').fadeOut('slow',function(){
					jQuery.get(url,function(data){
						jQuery('#modal-box').html(data);
						jQuery('#modal-box').fadeIn('slow');
						afterStudentPopupLoaded();
					});
				});
			});
			jQuery(".menu > li").click(function(e){
		switch(jQuery(e.target).attr('class')){
			case "gradebook":
				//change status & style menu
				jQuery(".gradebook").addClass("gradebook_active");
				jQuery(".discipline").removeClass("discipline_active");
				jQuery(".contacts").removeClass("contacts_active");
				jQuery(".report_card").removeClass("report_card_active");
				//display selected division, hide others
				jQuery("div.all").fadeIn();
				jQuery("div.date").css("display", "none");
				jQuery("div.type").css("display", "none");
				jQuery("div.four").css("display", "none");
			break;
			case "discipline":
				//change status & style menu
				jQuery(".gradebook").removeClass("gradebook_active");
				jQuery(".discipline").addClass("discipline_active");
				jQuery(".contacts").removeClass("contacts_active");
				jQuery(".report_card").removeClass("report_card_active");
				//display selected division, hide others
				jQuery("div.date").fadeIn();
				jQuery("div.all").css("display", "none");
				jQuery("div.type").css("display", "none");
				jQuery("div.four").css("display", "none");
				
				break;
			case "report_card":
				//change status & style menu
				jQuery(".gradebook").removeClass("gradebook_active");
				jQuery(".discipline").removeClass("discipline_active");
				jQuery(".contacts").removeClass("contacts_active");
				jQuery(".report_card").addClass("report_card_active");
				//display selected division, hide others
				jQuery("div.four").fadeIn();
				jQuery("div.all").css("display", "none");
				jQuery("div.type").css("display", "none");
				jQuery("div.date").css("display", "none");
			break;
			case "contacts":
				//change status & style menu
				jQuery(".gradebook").removeClass("gradebook_active");
				jQuery(".discipline").removeClass("discipline_active");
				jQuery(".contacts").addClass("contacts_active");
				jQuery(".report_card").removeClass("report_card_active");
				//display selected division, hide others
				jQuery("div.type").fadeIn();
				jQuery("div.all").css("display", "none");
				jQuery("div.date").css("display", "none");
				jQuery("div.four").css("display", "none");
			break;
		}
		//alert(e.target.id);
		return false;
	});
}



jQuery(function(){
	jQuery(".admin_student_link").click(function(e){
		var studentUrl = jQuery(e.target).parent().attr("data-student");
		jQuery.get(studentUrl, function(data){
			jQuery('#modal-box').html(data);
  			Modalbox.show($('modal-box'), {title: '&nbsp;', autoFocusing:false,  afterLoad: afterStudentPopupLoaded, width:972});
		});
	});
});