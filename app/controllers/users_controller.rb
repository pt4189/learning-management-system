class UsersController < ApplicationController
    before_filter :authenticate, :only => [:edit, :update, :dashboard]
    before_filter :correct_user, :only => [:edit, :update]
    helper_method :sort_column, :sort_direction
    respond_to :html, :js

  def index
    @users = User.order(:name)
    respond_to do |format|
      format.html
      format.json { render json: @users.json_tokens(params[:q]).map { |user|  {:id => user.id, :name => user.fullname} } }
    end
  end

  def course_index
    @users = params[:type] == 's' ? User.students : User.teachers
    respond_to do |format|
      format.json {
        render json: @users.as_json()
      }
    end
  end


  
  def show
    @user = User.find(params[:id])
  end
  
  def new
    
    @user = User.new
    @title="Sign up"
    @institutions =Institution.all
  end
  def update_iid
    
    @class = EduClass.find_all_by_institute_id(params[:id])
    render :update do |page|
      page.replace_html 'update3', :partial => 'users/update_iid', :collection => [@class]
    end  
  end
  
  def dashboard
     @users = User.all
     @student_assignments =  Assignment.find_all_by_class_id(current_user.class_id)
  end

  def create
     @user = User.new(params[:user])
     # multiselection functionality
     @user.subjects = Subject.find(params[:subject_ids]) if params[:subject_ids]
     if @user.save
      
       @class = User.create(:class_id => params[:user][:class_id])
      
       redirect_to index_admin_people_path(params[:user][:designation]) and return if current_user && current_user.admin?
       redirect_to user_root_path 
       
     else
       @title = "Sign up"
       render 'new'
     end
  end
  
  def student
    if current_user.role? :admin
    @users = User.order(sort_column + " " + sort_direction).search(params[:search]).find(:all, :conditions=>"designation='s'").paginate  :page => params[:page],:per_page => 10
    elsif current_user.role? :instituteadmin
    @users= User.order(sort_column + " " + sort_direction).search(params[:search]).find_all_by_designation_and_institute_id("s", current_user.institute_id).paginate  :page => params[:page],:per_page => 10
    end
  end
  
  def teacher
    if current_user.role? :admin
      @users = User.order(sort_column + " " + sort_direction).search(params[:search]).find(:all, :conditions=>"designation='t'").paginate  :page => params[:page],:per_page => 10
    elsif current_user.role? :instituteadmin
      @users= User.order(sort_column + " " + sort_direction).search(params[:search]).find_all_by_designation_and_institute_id("t", current_user.institute_id).paginate  :page => params[:page],:per_page => 10
    end
  end
  
  def create_student_info
   if request.post? and params[:user]
    if @user = User.new(params[:user])
      if @user.save
        SubjectsUser.create(subject_id: params[:s_subject_id], user_id: @user.id)
        unless params[:user][:photo].nil?
          redirect_to  course_student_infos(params[:s_subject_id])
        else
          respond_with(@user, location: course_student_infos_path(params[:s_subject_id]))
        end
      end
    end
  end
end

  
    def confirm_destroy
    
      respond_to do |format|
          format.js { render :action => 'confirm_destroy' }
      end
    end
  
  
    def confirm_setting      
         respond_to do |format|
          format.js {
            @user = User.find(current_user.id)
            @class_settings = @user.class_settings
           render :action => 'confirm_setting' 
         }
        end
    end
    
      
   def setting
      @user = current_user
      
      @user.update_column(:setting,params[:setting])    
      redirect_to root_path
   end

   def update_meta
    value = current_user.send(params[:user].first.first.to_sym)
    current_user.send("#{params[:user].first.first}=", params[:user].first.last)
    current_user.valid?
    value = params[:user].first.last if current_user.valid? && current_user.update_attribute(params[:user].first.first, params[:user].first.last)
    render :text => value
   end
  
  
  def new_student_info
    
  end
        
  def add_teacher
    @user = User.new
  end
  
  def edit_teacher
   @user = User.find(params[:id2])
  end
  
  def edit
    @title = "Edit user"
  end
   
  def edit_student
     @user = User.find(params[:id2])
  end
  def institute
    
    @users = User.find(:all, :conditions=>"designation='i'").paginate  :page => params[:page],:per_page => 10
  
  end
   def add_institute
        @user = User.new
  end
  def edit_institute
         @user = User.find(params[:id2])
  end
  def update
        @user = User.find(params[:id])
        # multiselection functionality
          @user.subjects = Subject.find(params[:subject_ids]) if params[:subject_ids]
    if @user.update_attributes(params[:user])
      flash[:success] = "Profile updated."
      redirect_to @user
   else
      @title = "Edit user"
      render 'edit'
   end
 end
 
  def destroy
      @user = User.find(params[:id])
      @user .destroy
      flash[:notice] = "Successfully destroyed ."
      redirect_to root_path
  end
  
  def check_email
    @user = User.find_by_email(params[:user][:email]) 
      respond_to do |format|
          format.json { render :json => !@user }
     end
  end
  
  def check_username
    @user = User.find_by_username(params[:user][:username]) 
      respond_to do |format|
          format.json { render :json => !@user }
     end
  end
 
  def change_password
    @user = User.find(current_user.id)
    if request.post?
      if User.authenticate(@user.username,
        params[:change_password][:old_password]) == @user
        @user.password = params[:change_password][:new_password]
        @user.password_confirmation =
        params[:change_password][:new_password_confirmation]
          if @user.validate_password && @user.save
            flash[:notice] = 'Password successfully update'
            request.xhr? ? render(:text => 'Password successfully updated') : redirect_to(change_password_path)
          else
            flash[:error] = 'New password mismatch'
            request.xhr? ? render(:text => 'Password '+ @user.errors.messages[:password].first) : render(:action => 'change_password')
          end
      else
          flash[:error] = 'Old password incorrect'
          request.xhr? ? render(:text => 'Old password incorrect') : render(:action =>'change_password')
       end
    end
   end
   
  
     

  private
    def authenticate
      deny_access unless signed_in?
    end
  
   def correct_user
    @user = User.find(params[:id])
    redirect_to(root_path) unless current_user=(@user)
   end
   
    def sort_column
         User.column_names.include?(params[:sort]) ? params[:sort] : "last_name"
    end

     def sort_direction
          %w[asc desc].include?(params[:direction]) ? params[:direction] : "asc"
     end

  
end
