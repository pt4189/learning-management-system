class CoursesController < ApplicationController
  before_filter  :authenticate ,:only => [:index,:subject]
  layout 'courses'

    def index
     if current_user.teacher? || current_user.student?
        @edu_classes = current_user.subjects
        
     elsif current_user.role? :instituteadmin
           @edu_classes =Subject.find_all_by_institute_id(current_user.institute_id)
     elsif current_user.role? :admin
        @edu_classes = Subject.find(:all)
      end
    end
    
    
    def subject
     if current_user.teacher? || current_user.student?
        @edu_classes = current_user.subjects
     elsif current_user.role? :instituteadmin
        @edu_classes = Subject.find_all_by_institute_id(current_user.id)
     elsif current_user.role? :admin
        @edu_classes = Subject.all
     end
    end
    
    private
      def authenticate
          deny_access unless signed_in?
      end


end
