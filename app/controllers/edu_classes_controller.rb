class EduClassesController < ApplicationController
  before_filter  :authenticate ,:only => [:edit,:update,:index,:create,:new]
  helper_method :sort_column, :sort_direction
  
    def index
      if current_user.role?:teacher
        
        @edu_classes = EduClass.order(sort_column + " " + sort_direction).find_all_by_institute_id(current_user.institute_id, :conditions => ['class_name LIKE ?', "%#{params[:search]}%"]).paginate  :page => params[:page],:per_page => 10
     elsif current_user.role? :instituteadmin
           @edu_classes = EduClass.order(sort_column + " " + sort_direction).find_all_by_institute_id(current_user.institute_id, :conditions => ['class_name LIKE ?', "%#{params[:search]}%"]).paginate  :page => params[:page],:per_page => 10
     else
        @edu_classes = EduClass.search(params[:search]).order(sort_column + " " + sort_direction).paginate  :page => params[:page],:per_page => 10
      end
    end


    def new
      @edu_class = EduClass.new
    end
 
    def create
      @edu_class =  EduClass.new(params[:edu_class])
      
      # multiselection functionality
       @edu_class.days = Day.find(params[:class_ids]) if params[:class_ids]
       
      if @edu_class.save
          students = params[:students].split(",")
          students.uniq.each {|student_id|
            student = User.find(student_id)
            student.class_id = @edu_class.id
            student.save!
          }
        redirect_to edu_classes_path
      else
        render :action => 'new'
      end
    end
  
    def show
    @edu_class = EduClass.find(params[:id])
    end
  
    def edit
      @edu_class = EduClass.find(params[:id])
    end
  
   def update
      @edu_class = EduClass.find(params[:id])
      # multiselection functionality
         @edu_class.days = Day.find(params[:class_ids]) if params[:class_ids]
         
        if @edu_class.update_attributes(params[:edu_class])
          to_be_added, to_be_removed = get_add_remove_ids(:students)
          
          to_be_added.uniq.each{|user_id|
            student = User.find(user_id)
            student.class_id = @edu_class.id
            student.save!
          }

          to_be_removed.uniq.each {|user_id|
            student = User.find(user_id)
            student.class_id = nil
            student.save!
          }

          redirect_to edu_classes_path
        else
          render :action => 'edit'
        end
  end  
  
  def destroy
    @edu_class = EduClass.find(params[:id])
    @edu_class.destroy
    flash[:notice] = "Successfully destroyed class."
    redirect_to  edu_classes_path
  end
  
  def toggled_status
    @edu_class = EduClass.find(params[:id])
    @edu_class.status = !@edu_class.status?
    @edu_class.save!
    redirect_to "/edu_classes"
  end 
  
  def students
    @users = EduClass.find(params[:id]).students
  end
  
  def teachers
    @staffs = Staff.find_all_by_class_id(params[:id])
  end
  
  def check_terms
    render :update do |page|
      page.replace_html 'term', :partial => 'edu_classes/check_terms'
    end
  end
  
  private
    def authenticate
        deny_access unless signed_in?
    end
    
     def sort_column
      EduClass.column_names.include?(params[:sort]) ? params[:sort] : "class_name"
    end

    def sort_direction
      %w[asc desc].include?(params[:direction]) ? params[:direction] : "asc"
    end

    def get_add_remove_ids(type)
      existing_ids = @edu_class.send(type).collect(&:id)
      params_data = params[type].split(",").uniq.collect{|a|a.to_i}
      to_be_removed = existing_ids - params_data
      to_be_added = params_data - existing_ids
      [to_be_added, to_be_removed]
    end
   
end
