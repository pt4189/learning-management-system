class CourseClassesController < ApplicationController
  before_filter  :authenticate ,:only => [:index, :check_assignments]
  helper_method :sort_column, :sort_direction
  
  layout 'courses'
  
  def index
    @assignments = Assignment.order(sort_column + " " + sort_direction).find_all_by_subject_id(params[:course_id], :conditions => ['name LIKE ?', "%#{params[:search]}%"])
    if ( current_user.role? :admin) || (current_user.role? :instituteadmin )
      @subjects = Subject.all #change -- Subject.find_all_by_class_id(params[:class_id])
    else
      @subjects = current_user.subjects
    end
  end
  
  def check_assignments
    @assignments = Assignment.find_all_by_subject_id(params[:course_id])
    
    render :update do |page|
        page.replace_html 'box_bg', :partial => 'course_classes/assignments'
    end
    
  end
  
  
  
  private
    def authenticate
        deny_access unless signed_in?
    end
    
     def sort_column
      Assignment.column_names.include?(params[:sort]) ? params[:sort] : "name"
    end

    def sort_direction
      %w[asc desc].include?(params[:direction]) ? params[:direction] : "asc"
    end
  
end
