class StudentInfosController < ApplicationController
  before_filter  :authenticate ,:only => [:index,:show,:check_student,:student,:result, :check_assignment ]
  include StudentInfosHelper
  
  layout 'courses'
  
  def index
     @subject = Subject.find(params[:course_id])
    
    if ( current_user.role? :admin) || (current_user.role? :instituteadmin )
      @subjects = Subject.all
    elsif current_user.teacher? || current_user.student?
      @subjects = current_user.subjects
    end
    
    @student_infos = @subject.students
    @student_profile = @subject.students.first
     @assignments = Assignment.find_all_by_subject_id(params[:course_id])
  end
  
  def show
    
      @student_infos = User.find(params[:id])
       @assignments = Assignment.find_all_by_subject_id(params[:subject])  
  end
   
   def check_student
     @subject = Subject.find(params[:course_id])
     @student_infos = @subject.students
     
     @student_profile = @subject.students.first
     @assignments = Assignment.find_all_by_subject_id(params[:course_id])
     
     
     render :update do |page|
         page.replace_html 'assign_stud', :partial => 'student_infos/assignmentstud'
         page.replace_html 'student_info_right', :partial => 'student_infos/student'
     end
   end
   
   def student
     @student_profile = User.find(params[:id])
     @subject = Subject.find(params[:course_id])
     @assignments = Assignment.find_all_by_subject_id(params[:course_id])  
     
      render :update do |page|
          page.replace_html 'student_info_right', :partial => 'student_infos/student'
      end
   end
   
   def check_assignment
     
     @user= StudentAssignment.find_all_by_user_id_and_assignment_id(params[:id],params[:assignment_id])
     @student_assignment =Assignment.find(params[:assignment_id])
      
   end
   
   def result
       @ques = StudentAssignment.update(params[:student_assignment].keys, params[:student_assignment].values).reject { |p| p.errors.empty? }
       
         result_calculator
       
       params[:course_class_id] = params[:assignment][:subject_id]
      
              
   end
   
   
   private
      def authenticate
          deny_access unless signed_in?
      end

end
