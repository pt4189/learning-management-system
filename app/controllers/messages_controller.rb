class MessagesController < ApplicationController
  include MessagesHelper
  respond_to :html, :js
  layout 'message'
  before_filter :authenticate, :only => [:index, :new]
  # GET /messages
  # GET /messages.json
  def index
    @messages = Message.paginate(:page => params[:page], :conditions=>["recipient = '#{current_user.id}' and is_deleted_by_recipient = false and subject LIKE ?", "%#{params[:search]}%"], :order=>"created_at DESC")
    respond_to do |format|
      format.html # index.html.erb
      format.json { render json: @messages }
    end
  end

  # GET /messages/1
  # GET /messages/1.json
  def show
      @messages = Message.find(params[:id], :conditions=>["recipient = '#{current_user.id}'"])
    @message = Message.find(params[:id])
    Message.update(@message.id, :is_read => true)

   
   
      respond_with(@message, location: messages_path)
  end

  # GET /messages/new
  # GET /messages/new.json
  def new
    @message = Message.new
    
  end

  # GET /messages/1/edit
  def edit
    @message = Message.find(params[:id])
  end

  # POST /messages
  # POST /messages.json
  def create
    
    recipients_array = params[:message][:user_tokens].split(",").collect{ |s| s.to_i }
    recipients_array.each do |r|
        user = User.find(r)
        @message = Message.new(:sender => current_user.id, :recipient => user.id, :subject=>params[:message][:subject],
                    :body=>params[:message][:body], :subject_id=>params[:message][:subject_id], :is_read=>false, :is_deleted_by_sender=>false,
                    :is_deleted_by_recipient=>false, :file_attachment=> params[:message][:file_attachment])
                    
        @message.save
        UserMailer.message_mailer(user, @message).deliver 
    end
    
      respond_with(@message, location: messages_path)
  end
  

  # PUT /messages/1
  # PUT /messages/1.json
  def update
    @message = Message.find(params[:id])

    respond_to do |format|
      if @message.update_attributes(params[:message])
        format.html { redirect_to @message, notice: 'Message was successfully updated.' }
        format.json { head :no_content }
      else
        format.html { render action: "edit" }
        format.json { render json: @message.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /messages/1
  # DELETE /messages/1.json
  def destroy
    @message = Message.find(params[:id])
    @message.destroy

    respond_to do |format|
      format.html { redirect_to messages_url }
      format.json { head :no_content }
    end
  end
  
  def subject
    @messages = Message.find_all_by_subject_id(params[:id],:conditions=>["recipient = '#{current_user.id}' and is_deleted_by_recipient = false "])
    
  end
   
   
   def forward
      
      @message = Message.find(params[:id])
      
      respond_with(@message, location: messages_path)
   end
   
   def reply
     @message = Message.find(params[:id])

       
        @message_body = "#{@message.body}"
       
   end    
  

  private
      def authenticate
          deny_access unless signed_in?
      end
end
