class SubTopicsController < ApplicationController
 before_filter  :authenticate ,:only => [:edit,:update,:index,:create,:new]

  
    def index
       
    end


   def new
       @subtopic = SubTopic.new
       respond_to do |format|
          format.js { render :action => 'new' }
        end
   end
 
   def create
       @subtopic= SubTopic.new(params[:sub_topic])
         if @subtopic.save
            @topic_id = @subtopic.topic_id
            @subtopics = SubTopic.find_all_by_topic_id(@subtopic.topic_id)
          else
            @error = true
          end
   end
  
  def show
     @sub_topic = SubTopic.find(params[:id])
  end
   
      

   def edit
     @subtopic = SubTopic.find(params[:id])
     respond_to do |format|
         format.js { render :action => 'edit' }
       end
    
    end

  
  def update
    @subtopic = SubTopic.find(params[:id])
    if @subtopic.update_attributes(params[:sub_topic])
      @topic_id = @subtopic.topic_id
      @subtopics = SubTopic.find_all_by_topic_id(@subtopic.topic_id)
    else
      @error = true
    end
  end  
  
  def destroy
    @subtopic = SubTopic.find(params[:id])
    @subtopic.destroy
    
  end
  
  
  def check_subtopic
     if current_user.role? :admin
         @topic_id = params[:topic_id]
         @subtopics = SubTopic.find_all_by_topic_id(@topic_id)
    elsif current_user.role? :instituteadmin
         @topic_id = params[:topic_id]
         @subtopics = SubTopic.find_all_by_institute_id_and_topic_id(current_user.institute_id,@topic_id)
    end  
    render :update do |page|
      if @subtopics.empty?
        page.replace_html 'subtopic', :partial => 'sub_topics/subtopics'
      else
        page.replace_html 'subtopic', :partial => 'sub_topics/subtopics'
      end

    end
  end

    private
    def authenticate
        deny_access unless signed_in?
    end
end
