class CircularList < Array
  def index
    @index ||=0
    @index.abs 
  end
  
  def current obj
    @index ||= (obj ? find_index(obj) : 0)
    # raise obj.inspect
    get_at(@index)
  end
  
  def next(num=1)
    @index ||= 0
    # @index += num
    get_at(@index+num)
  end
  
  def previous(num=1)
    @index ||= 0
    # @index -= num
    get_at(@index-num)
  end
  
  private
  def get_at(index)
    if index >= 0
      at(index % self.size)
    else
      index = self.size + index
      get_at(index)
    end
  end
  
end

class Admin::PeopleController < ApplicationController
	helper_method :sort_column, :sort_direction
	layout "admin_people"
	

	def index
    # raise params.inspect
		@users = User.order(sort_column + " " + sort_direction).search(params[:search]).find(:all, :conditions=>"designation='#{params[:type]}'")
    # @users = User.order(sort_column + " " + sort_direction).search(params[:search]).find(:all, :conditions=>"designation='t'").paginate  :page => params[:page],:per_page => 10
    # @users = User.find(:all, :conditions=>"designation='i'").paginate  :page => params[:page],:per_page => 10

	end

	def student_show
		@student_profile = User.find(params[:id])
		@users_array = User.order(sort_column + " " + sort_direction).search(params[:search]).find(:all, :conditions=>"designation='s'").collect(&:id)
		@user_list = CircularList.new(@users_array)
		# raise @user_list.inspect
		@user_list.current(params[:id].to_i)
		@grade = EduClass.find_by_id(@student_profile.class_id)
     	@subjects = Subject.find_all_by_class_id(@student_profile.class_id)
     	@subject = @subjects.first
     	@assignments = Assignment.find_all_by_subject_id(@subject.id) 
		render :student_show, layout: false
	end

	private
	def sort_column
		User.column_names.include?(params[:sort]) ? params[:sort] : "last_name"
	end

	def sort_direction
		%w[asc desc].include?(params[:direction]) ? params[:direction] : "asc"
	end

end