class SubjectsController < ApplicationController
    before_filter  :authenticate ,:only => [:edit,:update,:index,:create,:new]

  
    def index
      @subjects =  Subject.all if current_user.admin?
      @subjects = Subject.where("institute_id = #{current_user.id}") if current_user.instituteadmin?
      @subjects = current_user.subjects if current_user.student? or current_user.teacher?
    end

    def new
       @subject = Subject.new
       respond_to do |format|
          format.js { render :action => 'new' }
        end
    end
 
    def create
        @subject = Subject.new(params[:subject])
        @subject.days = Day.find(params[:day_ids]) if params[:day_ids]
        if @subject.save
          teachers = params[:teachers].split(",")
          students = params[:students].split(",")
          users = teachers + students
          users.uniq.each {|user_id|
            SubjectsUser.create(subject_id: @subject.id, user_id: user_id)
          }
          forum = Forem::Forum.new({name: "#{@subject.name}_forum", description: "Forum for #{@subject.name}", category_id: Forem::Category.first.id, for_id: @subject.id})
          forum.save!
           @subjects = Subject.all
         else
           @error = true
         end
    end
  
   def show
     @subject = Subject.find(params[:id])
   end
   
      

     def edit
     @subject = Subject.find(params[:id])
     respond_to do |format|
         format.js { render :action => 'edit' }
       end    
    end

  
    def update
        @subject = Subject.find(params[:id])
        @subject.days = Day.find(params[:day_ids]) if params[:day_ids]

        if @subject.update_attributes(params[:subject])
          add_teacher, remove_teacher = get_add_remove_ids(:teachers)
          add_student, remove_student = get_add_remove_ids(:students)
          to_be_added = add_teacher + add_student
          to_be_removed = remove_teacher + remove_student

          to_be_removed.uniq.each{|user_id|
            SubjectsUser.where("subject_id = #{@subject.id} and user_id = #{user_id}").delete_all
          }

          to_be_added.uniq.each {|user_id|
            SubjectsUser.create(subject_id: @subject.id, user_id: user_id)
          }

          @subjects = Subject.all
        else
          @error = true
        end
    end 
    
    def destroy
      @subject = Subject.find(params[:id])
      @subject.destroy
      flash[:notice] = "Successfully destroyed Subject."
      redirect_to subjects_path
    end
    
    def check_subject
      if current_user.role? :admin
         @class_id = params[:class_id]
         @subjects = Subject.find_all_by_class_id(@class_id)
      elsif current_user.role? :instituteadmin
        @class_id = params[:class_id]
        @subjects = Subject.find_all_by_institute_id_and_class_id(current_user.institute_id,@class_id)
      end
      render :update do |page|
        if @subjects.empty?
          page.replace_html 'subject', :partial => 'subjects/check_subject'
        else
          page.replace_html 'subject', :partial => 'subjects/check_subject'
        end

      end
    
    end
    def check_terms
      @subject = params[:id].nil? || params[:id] == "-1" ? Subject.new : Subject.find(params[:id])
      render :update do |page|
        page.replace_html 'term', :partial => 'check_terms'
      end
    end    

    private
    def authenticate
        deny_access unless signed_in?
    end

    def get_add_remove_ids(type)
      existing_ids = @subject.send(type).collect(&:id)
      params_data = params[type].split(",").uniq.collect{|a|a.to_i}
      to_be_removed = existing_ids - params_data
      to_be_added = params_data - existing_ids
      # raise "#{existing_ids.inspect} #{params_data.inspect} #{to_be_added.inspect} #{to_be_removed.inspect}" if type == :students

      [to_be_added, to_be_removed]
    end
   end