class SolvePdfAssignmentsController < ApplicationController
    respond_to :html, :js
  def new
    @assignment = Assignment.find(params[:student_assignment_id])
    @solve_pdf_assignment = SolvePdfAssignment.new
  end
  def create
     @solve_pdf_assignment = SolvePdfAssignment.new(params[:solve_pdf_assignment])
     if @solve_pdf_assignment.save
       flash[:notice] = "Successfully uploat."
     respond_with(@solve_pdf_assignment, location: root_path)
    else
      render :action => 'new'
    end
 
  end
end
