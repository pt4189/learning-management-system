class TopicsController < ApplicationController
  before_filter  :authenticate ,:only => [:edit,:update,:index,:create,:new]

  
       def index
         
       end


       def new
         @topic = Topic.new
          respond_to do |format|
             format.js { render :action => 'new' }
           end
       end
 
       def create
         @topic = Topic.new(params[:topic])
         if @topic.save
           @chapter_id = @topic.chapter_id
           @topics = Topic.find_all_by_chapter_id(@topic.chapter_id)
         else
           @error = true
         end
  end
  
     def show
     @topic = Topic.find(params[:id])
      
      end
   
      

     def edit
       @topic = Topic.find(params[:id])
       respond_to do |format|
               format.js { render :action => 'edit' }
       end
    
    end
    
    def check_topic
       if current_user.role? :admin
          @chapter_id = params[:chapter_id]
          @topics = Topic.find_all_by_chapter_id(@chapter_id)
       elsif current_user.role? :instituteadmin
          @chapter_id = params[:chapter_id]
          @topics = Topic.find_all_by_institute_id_and_chapter_id(current_user.institute_id,@chapter_id)
       end  
      render :update do |page|
        if @topics.empty?
          page.replace_html 'topic', :partial => 'topics/topics'
        else
          page.replace_html 'topic', :partial => 'topics/topics'
        end

      end
    end

  
      def update
        @topic = Topic.find(params[:id])

        if @topic.update_attributes(params[:topic])
          @chapter_id = @topic.chapter_id
          @topics = Topic.find_all_by_chapter_id(@topic.chapter_id)
        else
          @error = true
        end
  end  
    def destroy
    @topic = Topic.find(params[:id])
    @topic.destroy
    
  end

    private
    def authenticate
        deny_access unless signed_in?
    end
end
