class VideosController < ApplicationController

  respond_to :html, :js

  before_filter  :authenticate ,:only => [:edit,:update,:index,:create,:new, :show]

  before_filter :set_browser_type

  def index
    @videos = Video.find_all_by_subject_id(params[:course_id])

    respond_to do |format|
        format.js { render :action => 'index' }
    end
   
  end

	def show
    @video = Video.find(params[:id])

    @original_video = @video.panda_video
    @h264_encoding = @original_video.encodings["h264"]
    
  end

  def new
    @video = Video.new
  end

  def create
    @video = Video.create!(params[:video])

    redirect_to :controller=>"course_classes", :action=>"index", :course_id=> @video.subject_id
  end

   def edit
     @video = Video.find(params[:id])
   end

   def update
     
     @video = Video.find(params[:id])
      

        if @video.update_attributes(params[:video])

          redirect_to :controller=>"course_classes", :action=>"index", :course_id=> @video.subject_id

        end
   end

   def destroy
   @video = Video.find(params[:id])
   @panda_video = Panda::Video.find(@video.panda_video_id)
   @panda_video.delete
   @video.destroy
   respond_with(@video, location: videos_path)
   
   end

   private
    def authenticate
      deny_access unless signed_in?
    end
  
   def correct_user
    @user = User.find(params[:id])
    redirect_to(root_path) unless current_user=(@user)
   end

 def set_browser_type
   @browser_type = detect_browser
 end
 
  private
   
  MOBILE_BROWSERS = ["playbook", "windows phone", "android", "ipod", "iphone", "opera mini", "blackberry", "palm","hiptop","avantgo","plucker", "xiino","blazer","elaine", "windows ce; ppc;", "windows ce; smartphone;","windows ce; iemobile", "up.browser","up.link","mmp","symbian","smartphone", "midp","wap","vodafone","o2","pocket","kindle", "mobile","pda","psp","treo"]

  def detect_browser
    agent = request.headers["HTTP_USER_AGENT"].downcase
 
    MOBILE_BROWSERS.each do |m|
      return "mobile" if agent.match(m)
    end
    return "desktop"
  end

end
