class InstitutionsController < ApplicationController
  before_filter  :authenticate ,:only => [:edit,:update,:index,:create,:new]
   
    def index
      
       @institutions = Institution.all.paginate  :page => params[:page],:per_page => 10
         
      
    end


  def new
     @institution = Institution.new
  end
 
   def create
      @institution = Institution.new(params[:institution])
      if @institution.save
          redirect_to institutions_path
      else
          render :action => 'new'
      end
   end
  
  def show
    @institution = Institution.find(params[:id])
  end
  
  def edit
    @institution = Institution.find(params[:id])
  end
  
  def update
    @institution = Institution.find(params[:id])
    if @institution.update_attributes(params[:institution])
      redirect_to institutions_path
    else
      render :action => 'edit'
    end
  end  
  
  def destroy
    @institution = Institution.find(params[:id])
    @institution.destroy
    flash[:notice] = "Successfully destroyed product."
    redirect_to institutions_path
  end
  
  def check_terms
    render :update do |page|
      page.replace_html 'term', :partial => 'institutions/check_terms'
    end
  end
  
  
  private
    def authenticate
        deny_access unless signed_in?
    end
   
end
