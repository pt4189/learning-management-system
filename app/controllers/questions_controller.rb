class QuestionsController < ApplicationController
  before_filter  :authenticate ,:only => [:edit,:update,:index,:create,:new]

  
  def index
    @questions = Question.find_all_by_assignment_id(params[:assignment_id], :order => 'id desc')
  end

  def new
    @question = Question.new
       
     2.times do
      objquestion = @question.objquestions.build
     end
  end
 
  def create
    @question= Question.new(params[:question])
      if @question.save
        flash[:notice] = "Successfully created Question."
        if @question.type_of_question == 0
          
          redirect_to :controller=>"questions", :action=>"correct_answer", :course_id=> params[:course_id], :assignment_id=> @question.assignment_id, :id=> @question.id
          
        elsif @question.type_of_question == 1
          redirect_to course_assignment_questions_path
        end
      else
          render :action => 'new'
      end
  end
  
  def show
    @question = Question.find(params[:id])
     
     if request.post?
       add_options = Question.update(params[:question][:id], :correct_answer => params[:question][:correct_answer])
       redirect_to course_assignment_questions_path
     end
     
  end
  
  def correct_answer
    @question = Question.find(params[:id])
    if request.post? and params[:question][:option_answer]
       add_options = Question.update(params[:question][:id], :option_answer => params[:question][:option_answer])
       redirect_to course_assignment_questions_path
     end
  end
   
  def edit
     @question = Question.find(params[:id])
  end

  
  def update
    @question = Question.find(params[:id])
    if @question.update_attributes(params[:question])
       if @question.type_of_question == 0
          redirect_to :controller=>"questions", :action=>"correct_answer", :course_id=> params[:course_id], :assignment_id=> @question.assignment_id, :id=> @question.id
          
        elsif @question.type_of_question == 1
          redirect_to course_assignment_questions_path
        end
    else
      render :action => 'edit'
    end
  end  
  
  def destroy
    @question = Question.find(params[:id])
    @question.destroy
    flash[:notice] = "Successfully destroyed question."
    redirect_to course_assignment_questions_path
  end
  
 

    private
    def authenticate
        deny_access unless signed_in?
    end
end
