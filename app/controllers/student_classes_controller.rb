class StudentClassesController < ApplicationController
  before_filter  :authenticate ,:only => [:edit,:update,:index,:create,:new]
  helper_method :sort_column, :sort_direction
  
  def check_student_class
      if current_user.role? :admin
              @staffs = User.order(sort_column + " " + sort_direction).find_all_by_class_id(params[:class_id])
      elsif current_user.role? :instituteadmin
         @staffs = User.order(sort_column + " " + sort_direction).find_all_by_class_id_and_institute_id_and_designation(params[:class_id],current_user.institute_id,"s")
      end   
   end
   
   def toggled_status
     @staff = User.find(params[:id])
     @staff.class_status = !@staff.class_status?
     @staff.save!
     
     @staffs = User.find_all_by_class_id(@staff.class_id)

     render :update do |page|
       if @staffs.empty?
          page.replace_html 'student', :partial => 'student_classes/student'
        else
          page.replace_html 'student', :partial => 'student_classes/student'
        end

     end
   end
   
   
    private
    def authenticate
         deny_access unless signed_in?
    end
    
    def sort_column
         User.column_names.include?(params[:sort]) ? params[:sort] : "last_name"
    end

    def sort_direction
        %w[asc desc].include?(params[:direction]) ? params[:direction] : "asc"
    end
   
end
