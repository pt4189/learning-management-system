class AssignQuesController < ApplicationController
  respond_to :html, :js
  before_filter  :authenticate ,:only => [:create,:new]
  
  def new
    @student_assignment = StudentAssignment.new
    @ques = Question.find_all_by_assignment_id(params[:student_assignment_id]) 
  end
  
  def create
     @ques = StudentAssignment.create(params[:student_assignment].values).reject { |p| p.errors.empty? }
     
     @save= SolvedStudent.create(:user_id=>params[:assignment][:user_id], :assignment_id=>params[:assignment][:assignment_id])
      if @ques.empty?
        flash[:notice] = "Assignment posted"
        respond_with(@ques, location: root_path)
      else
        render :action => "new"
      end
  end
  
  private
  def authenticate
      deny_access unless signed_in?
  end
  
end
