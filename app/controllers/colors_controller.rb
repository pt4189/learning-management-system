class ColorsController < ApplicationController
  before_filter  :authenticate ,:only => [:edit,:update,:index,:create,:new]

  
       def index
       @colors = Color.find(:all)
       end


       def new
       @color = Color.new
       end
 
       def create
       @color= Color.new(params[:color])
         if@color.save
         flash[:notice] = "Successfully created Chapter."
          redirect_to colors_path
    else
      render :action => 'new'
    end
  end
  
     def show
     @color = Color.find(params[:id])
      
      end
   
      

     def edit
     @color = Color.find(params[:id])
    
    end

  
      def update
    @color = Color.find(params[:id])
    if @color.update_attributes(params[:color])
      redirect_to colors_path
    else
      render :action => 'edit'
    end
  end  
    def destroy
    @color = Color.find(params[:id])
    @color.destroy
    flash[:notice] = "Successfully destroyed Chapter."
    redirect_to colors_path
  end

    private
    def authenticate
        deny_access unless signed_in?
    end
end
