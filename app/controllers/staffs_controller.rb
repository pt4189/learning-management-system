class StaffsController < ApplicationController
  before_filter  :authenticate ,:only => [:edit,:update,:index,:create,:new]
  helper_method :sort_column, :sort_direction
  
  def index
  end
  
  def new
     @staff = Staff.new
      respond_to do |format|
         format.js { render :action => 'new' }
       end
   end

  # Admin and Institute can create a staff
   def create
     @staff = Staff.new(params[:staff])
     if @staff.save
       @class_id = @staff.class_id
       @staffs = User.joins(:staffs).select("users.first_name, users.last_name, staffs.status, staffs.id").where('staffs.class_id' => @class_id)

        render :update do |page|         
             page.replace_html 'staffs', :partial => 'staffs/staff'
             page << "Modalbox.hide();"
      end
      else
       @error = true
      end
   end
  
  def check_class
    #Admin manage all staffs and sort column
    if current_user.role? :admin
         @class_id = params[:class_id]
         @staffs = User.order(sort_column + " " + sort_direction).joins(:staffs).select("users.first_name, users.last_name, staffs.status, staffs.id").where('staffs.class_id' => @class_id)
    #Institute can view his staffs 
    elsif current_user.role? :instituteadmin
         @class_id = params[:class_id]
         @staffs = User.order(sort_column + " " + sort_direction).joins(:staffs).select("users.first_name, users.last_name, staffs.status, staffs.id").where('institute_id'=>current_user.institute_id ,'staffs.class_id' => @class_id)
         
    end  
      render :update do |page|
        if @staffs.empty?
          page.replace_html 'staffs', :partial => 'staffs/staff'
        else
          page.replace_html 'staffs', :partial => 'staffs/staff'
        end

    end
  end
  
  
  # Admin and Institute toggled staff status
  def toggled_status
    @staff = Staff.find(params[:id])
    @staff.status = !@staff.status?
    @staff.save!
    @class_id = @staff.class_id
    @staffs = User.joins(:staffs).select("users.first_name, users.last_name, staffs.status, staffs.id").where('staffs.class_id' => @class_id)
    render :update do |page|
      if @staffs.empty?
        page.replace_html 'staffs', :partial => 'staffs/staff'
      else
        page.replace_html 'staffs', :partial => 'staffs/staff'
      end

    end
  end
  
  private
  # Check Authentication If user signed in or not?
  def authenticate
       deny_access unless signed_in?
  end
  
  # Sort column
  def sort_column
       User.column_names.include?(params[:sort]) ? params[:sort] : "last_name"
  end

  def sort_direction
      %w[asc desc].include?(params[:direction]) ? params[:direction] : "asc"
  end
  
end
