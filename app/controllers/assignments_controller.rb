class AssignmentsController < ApplicationController
  
  respond_to :html, :js
  
  before_filter  :authenticate ,:only => [:edit,:update,:index,:create,:new, :submit_assignment]
  
  def index
    @assignments = Assignment.find_all_by_class_id(params[:edu_class_id], :order => 'id desc')
  end
   
  def new
    @assignment = Assignment.new
    @subject = Subject.find(params[:course_id])
    @unit = []
    @chapter = []
    @topic = []
    @subtopic = []
    
  end
  

  def create
    
    @assignment = Assignment.new(params[:assignment])
    if @assignment.save
      @assignment.update_attribute(:color_code, current_user.get_color_for(@assignment.subject_id))
      flash[:notice] = "Assignments created successfully"
      respond_with(@assignment, location: course_assignments_path)
    else
      render :new
    end
  end
  
  def edit
    @assignment = Assignment.find(params[:id])
    @subject = Subject.find(params[:course_id])
    @unit = Unit.find_all_by_subject_id(@assignment.subject_id)
    @chapter = Chapter.find_all_by_unit_id(@assignment.unit_id)
    @topic = Topic.find_all_by_chapter_id(@assignment.chapter_id)
    @subtopic = SubTopic.find_all_by_topic_id(@assignment.topic_id)
  end

  def update
    @assignment = Assignment.find(params[:id])
    if @assignment.update_attributes(params[:assignment])
      @assignment.update_attribute(:color_code, current_user.get_color_for(@assignment.subject_id))
      flash[:notice] = "Assignments saved successfully"
      redirect_to :controller=>"assignments", :action=>"index", :edu_class_id=>@assignment.class_id
    else
      render :new
    end
  end
  

  def destroy
    @assignment = Assignment.find(params[:id])
   
    if @assignment.destroy
      flash[:notice] = "Assignments deleted successfully"
      redirect_to edu_class_assignments_path
    end
    
  end
  
  def check_unit
    @unit = Unit.find_all_by_subject_id(params[:subject_id])
    render :update do |page|
      page.replace_html 'unit', :partial => 'assignments/check_unit', :object => @unit  
    end
  end
  
  def check_chapter
    @chapter = Chapter.find_all_by_unit_id(params[:unit_id])
    render :update do |page|
      page.replace_html 'chapter', :partial => 'assignments/check_chapter', :object => @chapter  
    end
  end
  def check_duedate
   @subject = Subject.find(params[:course_id]) 
   # @edu_class = EduClass.find(@subject.class_id)
     
    render :update do |page|
      page.replace_html 'date_container', :partial => 'assignments/check_duedate', :object => @subject  
    end
  end
  def check_topic
    @topic = Topic.find_all_by_chapter_id(params[:chapter_id])
    render :update do |page|
      page.replace_html 'topic', :partial => 'assignments/check_topic', :object => @topic  
    end
  end
  
   def pdf_upload
     
    render :update do |page|
      page.replace_html 'assignment_type', :partial => 'assignments/pdf_upload' 
    end
  end
  
  
  def check_subtopic
     @subtopic = SubTopic.find_all_by_topic_id(params[:topic_id])
      render :update do |page|
        page.replace_html 'subtopic', :partial => 'assignments/check_subtopic', :object => @subtopic  
      end
  end
  def student
     @student= User.find_all_by_class_id_and_designation(params[:edu_class_id],"s")
   end
  def submit_assignment
    @user= StudentAssignment.find_all_by_user_id_and_assignment_id(params[:id],params[:assignment_id])
    
    if request.post?
      @ques = StudentAssignment.update(params[:student_assignment].keys, params[:student_assignment].values).reject { |p| p.errors.empty? }
      redirect_to("/")
    end
    
  end
  def calender_asses
   @events = Assignment.scoped  
    @events = @events.after(params['start']) if (params['start'])
    @events = @events.before(params['end']) if (params['end'])

    respond_to do |format|
      format.html # index.html.erb
      format.json { render json: @events }
    end
  end
  
  def filter
         
 @assignments = Assignment.where('subject_id = ?  AND created_at > ? AND created_at < ?',params[:subject_id],Assignment.format_date(params[:start]),Assignment.format_date(params[:end]))
          
    respond_to do |format|
      format.html # index.html.erb
      format.json { render json: @assignments }
    end
  end 
  
  
    
  private
  def authenticate
      deny_access unless signed_in?
  end
  
end