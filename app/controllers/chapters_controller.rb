class ChaptersController < ApplicationController
    before_filter  :authenticate ,:only => [:edit,:update,:index,:create,:new]

  
       def index
      
       end


       def new
         @chapter = Chapter.new
          respond_to do |format|
             format.js { render :action => 'new' }
           end
       end
 
       def create
         @chapter = Chapter.new(params[:chapter])
         if @chapter.save
           @unit_id = @chapter.unit_id
           @chapters = Chapter.find_all_by_unit_id(@chapter.unit_id)
         else
           @error = true
         end
  end
  
     def show
     @chapter = Chapter.find(params[:id])
      
      end
      
      def check_chapter
         if current_user.role? :admin
        @unit_id = params[:unit_id]
        @chapters = Chapter.find_all_by_unit_id(@unit_id)
        elsif current_user.role? :instituteadmin
           @unit_id = params[:unit_id]
           @chapters = Chapter.find_all_by_institute_id_and_unit_id(current_user.institute_id,@unit_id)
        end   
        render :update do |page|
          if @chapters.empty?
            page.replace_html 'chapter', :partial => 'chapters/chapters'
          else
            page.replace_html 'chapter', :partial => 'chapters/chapters'
          end

        end
      end
      

      def edit
        @chapter = Chapter.find(params[:id])
        respond_to do |format|
                format.js { render :action => 'edit' }
        end
      end
  
      def update
        @chapter = Chapter.find(params[:id])

        if @chapter.update_attributes(params[:chapter])
          @unit_id = @chapter.unit_id
          @chapters = Chapter.find_all_by_unit_id(@chapter.unit_id)
        else
          @error = true
        end
      end
      
    def destroy
    @chapter = Chapter.find(params[:id])
    @chapter.destroy
    
  end

    private
    def authenticate
        deny_access unless signed_in?
    end
end
