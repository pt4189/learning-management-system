class UnitsController < ApplicationController
   before_filter  :authenticate ,:only => [:edit,:update,:index,:create,:new]

  def index
     
  end

  def new
    @unit = Unit.new
     respond_to do |format|
        format.js { render :action => 'new' }
      end
  end
 
  def create
    @unit = Unit.new(params[:unit])
    if @unit.save
      @subject_id = @unit.subject_id
      @units = Unit.find_all_by_subject_id(@unit.subject_id)
    else
      @error = true
    end
  end
  
  def show
    @unit = Unit.find(params[:id])
    
  end
   
      

  def edit
    @unit = Unit.find(params[:id])
    respond_to do |format|
            format.js { render :action => 'edit' }
    end
  end

  
  def update
    @unit = Unit.find(params[:id])
    
    if @unit.update_attributes(params[:unit])
      @subject_id = @unit.subject_id
      @units = Unit.find_all_by_subject_id(@unit.subject_id)
    else
      @error = true
    end
  end 
  
  def check_unit
    if current_user.role? :admin
    @subject_id = params[:subject_id]
    @units = Unit.find_all_by_subject_id(@subject_id)
     elsif current_user.role? :instituteadmin
        @subject_id = params[:subject_id]
        @units = Unit.find_all_by_institute_id_and_subject_id(current_user.institute_id,@subject_id)
    end
    render :update do |page|
      if @units.empty?
        page.replace_html 'unit', :partial => 'units/units'
      else
        page.replace_html 'unit', :partial => 'units/units'
      end
      
    end
  end
   
  def destroy
   @unit = Unit.find(params[:id])
   @unit.destroy
    
  end

    private
    def authenticate
        deny_access unless signed_in?
    end
end
