class ClassSettingsController < ApplicationController
	def update
		if class_setting.update_attribute(params[:remainder_for].to_sym, params[:value])
			render :nothing => true, :status => 200
		else
			render :status => 500, :nothing => true
		end
	end

	def update_color
		data = class_setting.update_color(params[:value])
		render :text => data
	end

	def update_remainder
		if class_setting.update_remainder(params[:remainder_for], params[:remainder_type], params[:value])
			render :nothing => true, :status => 200
		else
			render :status => 500, :nothing => true
		end
	end

	def class_setting
		ClassSetting.find(params["class_setting_id"])
	end
end