class CalendersController < ApplicationController
  layout 'inner'
  before_filter :authenticate, :only => [:index, :show, :view_assignment]
  
  def show
  end
  
  def view_assignment
       @assignments = Assignment.find_all_by_id(params[:id])

  end
  
  
  private
    def authenticate
      deny_access unless signed_in?
    end
end
