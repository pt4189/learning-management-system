class SubQuestionsController < ApplicationController
  before_filter  :authenticate ,:only => [:edit,:update,:index,:create,:new]

  
  def index
     @sub_questions = SubQuestion.find_all_by_question_id(params[:question_id], :order => 'id desc')
  end


  def new
    @sub_question = SubQuestion.new
    2.times do
      subobjquestion = @sub_question.subobjquestions.build
     end
  end
 
  def create
    @sub_question= SubQuestion.new(params[:sub_question])
         
    if @sub_question.save
      flash[:notice] = "Successfully created SubQuestion."
      
      if @sub_question.type_of_question == 0
        
        redirect_to :controller=>"sub_questions", :action=>"correct_answer", :edu_class_id=> @sub_question.class_id, :assignment_id=> @sub_question.assignment_id, :question_id=>@sub_question.question_id, :id=> @sub_question.id
        
      elsif @sub_question.type_of_question == 1
        redirect_to edu_class_assignment_question_sub_questions_path
      end
      
    else
      render :action => 'new'
    end
  end
  
     
  def show
    @sub_question = SubQuestion.find(params[:id])
  end
  
  def correct_answer
     @sub_question = SubQuestion.find(params[:id])
     if request.post? and params[:question][:option_answer]
        add_options = SubQuestion.update(params[:question][:id], :option_answer => params[:question][:option_answer])
        redirect_to edu_class_assignment_question_sub_questions_path
      end
   end
   
      

  def edit
     @sub_question = SubQuestion.find(params[:id])
  end

  
  def update
    @sub_question = SubQuestion.find(params[:id])
    if @sub_question.update_attributes(params[:sub_question])
      
      if @sub_question.type_of_question == 0
        
        redirect_to :controller=>"sub_questions", :action=>"correct_answer", :edu_class_id=> @sub_question.class_id, :assignment_id=> @sub_question.assignment_id, :question_id=>@sub_question.question_id, :id=> @sub_question.id
        
      elsif @sub_question.type_of_question == 1
        redirect_to edu_class_assignment_question_sub_questions_path
      end
      
    else
      render :action => 'edit'
    end
  end  
    
  def destroy
    @sub_question = SubQuestion.find(params[:id])
    @sub_question.destroy
    flash[:notice] = "Successfully destroyed SubQuestion."
    redirect_to edu_class_assignment_question_sub_questions_path
  end

    private
    def authenticate
        deny_access unless signed_in?
    end
end
