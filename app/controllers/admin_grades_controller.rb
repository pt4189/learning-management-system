class AdminGradesController < ApplicationController
  # GET /admin_grades
  # GET /admin_grades.json
  def index
    @admin_grades = AdminGrade.all

    respond_to do |format|
      format.html # index.html.erb
      format.json { render json: @admin_grades }
    end
  end

  # GET /admin_grades/1
  # GET /admin_grades/1.json
  def show
    @admin_grade = AdminGrade.find(params[:id])

    respond_to do |format|
      format.html # show.html.erb
      format.json { render json: @admin_grade }
    end
  end

  # GET /admin_grades/new
  # GET /admin_grades/new.json
  def new
    @admin_grade = AdminGrade.new

    respond_to do |format|
      format.html # new.html.erb
      format.json { render json: @admin_grade }
    end
  end

  # GET /admin_grades/1/edit
  def edit
    @admin_grade = AdminGrade.find(params[:id])
  end

  # POST /admin_grades
  # POST /admin_grades.json
  def create
    @admin_grade = AdminGrade.new(params[:admin_grade])

    respond_to do |format|
      if @admin_grade.save
        format.html { redirect_to @admin_grade, notice: 'Admin grade was successfully created.' }
        format.json { render json: @admin_grade, status: :created, location: @admin_grade }
      else
        format.html { render action: "new" }
        format.json { render json: @admin_grade.errors, status: :unprocessable_entity }
      end
    end
  end

  # PUT /admin_grades/1
  # PUT /admin_grades/1.json
  def update
    @admin_grade = AdminGrade.find(params[:id])

    respond_to do |format|
      if @admin_grade.update_attributes(params[:admin_grade])
        format.html { redirect_to @admin_grade, notice: 'Admin grade was successfully updated.' }
        format.json { head :no_content }
      else
        format.html { render action: "edit" }
        format.json { render json: @admin_grade.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /admin_grades/1
  # DELETE /admin_grades/1.json
  def destroy
    @admin_grade = AdminGrade.find(params[:id])
    @admin_grade.destroy

    respond_to do |format|
      format.html { redirect_to admin_grades_url }
      format.json { head :no_content }
    end
  end
end
