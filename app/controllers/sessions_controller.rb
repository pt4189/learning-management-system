class SessionsController < ApplicationController
  def new
    if signed_in?
      redirect_to("/dashboard")
    else
      @title="Sign in"
    end
    
  end
  def create
    user = User.authenticate(params[:session][:username], params[:session][:password])
    if user.nil?
    flash.now[:error] = "Invalid username/password combination."
    @title = "Sign in"
    render 'new'
    else
      sign_in user
      redirect_to user_root_path
     end
 end
 
  def user_signout
    if  params[:assignment][:dont_ask] == '1'
      @user = User.find(current_user.id)
      @user.update_column(:dont_ask, true)
    end
    sign_out
    redirect_to root_path
  end
  
  def destroy
    
    sign_out
    redirect_to root_path
  end

end
