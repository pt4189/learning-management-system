Forem::TopicsController.class_eval do
	def create
		authorize! :create_topic, @forum
		@topic = @forum.topics.build(params[:topic], :as => :default)
		@topic.user = forem_user
		@topic.save  
		@index = @forum.topics.count
	end

	def show
		if find_topic
			register_view
			@posts = @topic.posts
			respond_to do |format|
				format.js { render :action => 'show' }
			end
		end
	end

	def destroy
		@topic = @forum.topics.find(params[:id])
		@topic.destroy
		@forum.reload
	end

	def find_topic
      begin
        @topic = Forem::Topic.find(params[:id])
        authorize! :read, @topic
      rescue ActiveRecord::RecordNotFound
        flash.alert = t("forem.topic.not_found")
        redirect_to @forum and return
      end
    end
end