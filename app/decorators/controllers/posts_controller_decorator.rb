Forem::PostsController.class_eval do
	def destroy
		@post = @topic.posts.find(params[:id])
		@id = params[:id]
		@post.destroy if @post.user == forem_user || forem_user.teacher?
	end
end