Forem::ForumsController.class_eval do
  def show
      @forum = Forem::Forum.find(params[:id])
      respond_to do |format|
        format.js { render :action => 'show' }
       end
    end
end