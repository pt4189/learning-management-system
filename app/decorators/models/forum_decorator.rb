Forem::Forum.class_eval do
	attr_accessible :for_id, :name, :description, :category_id

	def for
		Subject.find(self.for_id)
	end
end