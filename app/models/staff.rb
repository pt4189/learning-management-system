class Staff < ActiveRecord::Base
  belongs_to :user
  belongs_to :edu_class, :class_name => "EduClass", :foreign_key => "class_id"
  delegate :class_name,  :to => :edu_class
  delegate :color, :automatic_remainder, :to => :class_setting
  attr_accessible :class_id, :status, :user_id,:institute_id
  validates_uniqueness_of :class_id , :scope => :user_id
end