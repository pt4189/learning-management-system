class Subject < ActiveRecord::Base
   attr_accessible :name,:status, :institute_id, :start_time, :end_time, :subject_location, 
   :terms, :year, :sem, :seats, :code, :credits, :description
   validates_presence_of :name

   has_one :forum, foreign_key: "for_id", class_name: Forem::Forum
   
   has_and_belongs_to_many :days
   has_and_belongs_to_many :users
   
   def teachers
    teachers = users.where("designation='t'") + users.collect{|u| u if u.teacher? }.compact
    teachers.uniq
   end

   def students
    students = users.where("designation='s'") + users.collect{|u| u if u.student? }.compact
    students.uniq
   end

   def build_forum
    Forem::Forum.new({name: "#{self.name}_forum", description: "Forum for #{self.name}", category_id: Forem::Category.first.id, for_id: self.id})
   end

   def self.create_forum_for_existing_records
    all.each do |sub|
      sub.build_forum.save! if sub.forum.nil?
    end
   end

end
