class SubjectsUser < ActiveRecord::Base
	attr_accessible :subject_id, :user_id
	belongs_to :subject
	belongs_to :user
	has_one :class_setting
	after_create :create_class_setting

	def create_class_setting
		class_setting = ClassSetting.new
		class_setting.subjects_user_id = id
		class_setting.save!
	end

	def class_setting
		ClassSetting.where(:subjects_user_id => id).first
	end

	def has_remainder(remainder_for, remainder)
		class_setting.has_remainder(remainder_for, remainder)
	end
end