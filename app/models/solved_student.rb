class SolvedStudent < ActiveRecord::Base
  attr_accessible :user_id, :assignment_id
  
  validates_uniqueness_of :user_id, :scope => [ :assignment_id]
end
