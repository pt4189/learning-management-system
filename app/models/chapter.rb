class Chapter < ActiveRecord::Base
   attr_accessible :name, :unit_id,:status,:institute_id
   validates_presence_of :name
end
