class Video < ActiveRecord::Base
  # attr_accessible :title, :body
  attr_accessible :class_id, :panda_video_id, :subject_id, :title, :user_id
    validates_presence_of :panda_video_id

  def panda_video
    @panda_video ||= Panda::Video.find(panda_video_id)
  end
end
