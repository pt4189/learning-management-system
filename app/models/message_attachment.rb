class MessageAttachment < ActiveRecord::Base
  belongs_to :message
  
  attr_accessible :message_id, :attachment, :name
  
    
end
