class SubQuestion < ActiveRecord::Base
  
    has_many :subobjquestions, :dependent => :destroy
    accepts_nested_attributes_for :subobjquestions, :reject_if => lambda { |a| a[:que].blank? }, :allow_destroy => true
    
    attr_accessible :subquestion,:type_of_question,:option,:explaination,:correct_answer,:question_id,:status, :subobjquestions_attributes, :class_id, :assignment_id, :user_id

end
