class Event < ActiveRecord::Base
  attr_accessible :date, :location, :name, :notes

  scope :before, lambda {|end_time| {:conditions => ["date < ?", Event.format_date(end_time)] }}
  scope :after, lambda {|start_time| {:conditions => ["date > ?", Event.format_date(start_time)] }}
  
  # need to override the json view to return what full_calendar is expecting.
  # http://arshaw.com/fullcalendar/docs/event_data/Event_Object/
  def as_json(options = {})
    {
      :id => self.id,
      :title => self.name,
      
      :start => date.rfc822,
      :end => date.rfc822,
      
      :recurring => false,
      :url => Rails.application.routes.url_helpers.event_path(id),
      :color => "#378006"
    }
    
  end
  
  def self.format_date(date_time)
    Time.at(date_time.to_i).to_formatted_s(:db)
  end
  
  def mail_event_to_every_institute_user(current_user)
      event_user = User.find_all_by_institute_id(current_user.institute_id)
      UserMailer.event_mailer(event_user, self).deliver
  end
  
end
