class SolvePdfAssignment < ActiveRecord::Base
  attr_accessible :std_pdf,:class_id,:subject_id,:assignment_id,:user_id
   has_attached_file :std_pdf,   :storage => :s3, :s3_credentials => "#{Rails.root}/config/s3.yml",
                    :path => "std_pdf/:id/:style/:basename.:extension"
    
end
