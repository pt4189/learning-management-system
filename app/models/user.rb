
require 'digest'


class User < ActiveRecord::Base
  attr_accessor :password

  attr_accessible :first_name, :last_name, :name, :email, :password, :password_confirmation,:role_ids, :user_id,:class_id,:designation, :username, :class_status,:institute_id,:photo,:dont_ask, :language
  
   has_and_belongs_to_many :roles
   has_many :subjects_user
   has_and_belongs_to_many :subjects
   has_many :class_settings, through: :subjects_user

   belongs_to :edu_class, foreign_key: "class_id"

   has_many :staffs
   
  validates :first_name, :presence => true, 
                  :length => { :maximum => 50 }
  validates :last_name, :presence => true, 
                  :length => { :maximum => 50 }
  email_regex = /\A[\w+\-.]+@[a-z\d\-.]+\.[a-z]+\z/i

  validates :email, :presence => true,
                     :format => { :with => email_regex},
                     :uniqueness => { :case_sensitive => false }
  
  validates :username, :presence => true, :uniqueness => { :case_sensitive => false }
  validates :password,  :confirmation => true
before_save :encrypt_password
before_create { generate_token(:auth_token) }

has_attached_file :photo, :styles =>{:small => "92x188>" }


  def self.json_tokens(query)
    users = where("LOWER(first_name) like ? OR LOWER(last_name) LIKE ?", "%#{query}%", "%#{query}%")
    if users.empty?
    else
      users
    end
  end

  def class_name
    edu_class.nil? ? "" : edu_class.class_name
  end
  
  def self.recipient_tokens(query)
    users = User.select("users.*").where("id = #{query}")
    if users.empty?
    else
      users
    end
  end
  
  def fullname
    "#{first_name} #{last_name}"
  end

  def get_color_for subject_id

    subject_user_id = subjects_user.find_by_subject_id(subject_id).id

    ClassSetting.find_by_subjects_user_id(subject_user_id).color
  end

  def check_message
      messages =[]
      messages = Message.find(:all , :conditions => ["recipient = '#{self.id}'"])
      count = 0
      messages.each do |r|
        unless r.is_read
          count += 1
        end
      end
      return count
  end

  def self.students
    students = where("designation='s'") + all.collect{|u| u if u.student? }.compact
    students.uniq.sort_by{|u| u.fullname}
  end

  def self.teachers
    teachers = where("designation='t'") + all.collect{|u| u if u.teacher? }.compact
    teachers.uniq.sort_by{|u| u.fullname}
  end

def send_password_reset
  generate_token(:password_reset_token)
  self.password_reset_sent_at = Time.zone.now
  save!
  UserMailer.password_reset(self).deliver
end

def generate_token(column)
  begin
    self[column] = SecureRandom.urlsafe_base64
  end while User.exists?(column => self[column])
end
def has_password?(submitted_password)
  encrypted_password == encrypt(submitted_password)
end
class << self
def authenticate(username, submitted_password)
user = find_by_username(username)
(user && user.has_password?(submitted_password)) ? user :nil

end
def authenticate_with_salt(id,cookie_salt)
  user = find_by_id(id)
  (user && user.salt == cookie_salt) ? user :nil
end 
end

 def role?(role)
    return !!self.roles.find_by_name(role.to_s)
  end

  def admin?
    role? :admin
  end
  def instituteadmin?
    role? :instituteadmin
  end

  def teacher?
    role? :teacher
  end
  def student?
    role? :student
  end
  
  
  def self.search(search)
    if search
      where('first_name LIKE ?', "%#{search}%")
    else
      scoped
    end
  end

  def edu_classes
    return staffs if role?("teacher")
    return ClassSetting.where(:staff_id => id, :entity_type => "STUDENT") if role?("student")
    []
  end


  def validate_password
    errors.add(:password, "should be minimum 6 chars") if password.length < 6
    password.length >= 6
  end
  
  def name_label
    name = self.class_name.blank? ? self.fullname : "#{self.fullname} - #{self.class_name}"
  end
  
  def as_json(options={})
    
    { :email => self.email, label: self.name_label, value: self.id }
  end
  
private
def encrypt_password
self.salt = make_salt if new_record?
self.encrypted_password = encrypt(password)
end
def encrypt(string)
secure_hash("#{salt}--#{string}")
end
def make_salt
secure_hash("#{Time.now.utc}--#{password}")
end
def secure_hash(string)
Digest::SHA2.hexdigest(string)
end


end
