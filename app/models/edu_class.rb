class EduClass < ActiveRecord::Base
	attr_accessible :class_name, :class_days,:class_hours,:class_location,:no_of_seats,:user_id,:status,:institute_id, :start_time, :end_time, :terms, :year, :sem, :graduation_month, :graduation_year

    # join relationship for categories and products model
    has_and_belongs_to_many :days
    has_many :users, :foreign_key => 'class_id'

    def self.search(search)
    	if search
    		where('class_name LIKE ?', "%#{search}%")
    	else
    		scoped
    	end
    end

    def students
    	students = users.where("designation='s'") + users.collect{|u| u if u.student? }.compact
        students.uniq
    end
end
