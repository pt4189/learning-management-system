class Assignment < ActiveRecord::Base
  has_many :questions
  attr_accessible :assignment_type, :class_id, :due_date, :max_point, :name, :subject_id, :unit_id, :chapter_id, :topic_id, :sub_topic_id, :user_id,:pdf,:color_code, :notes
    has_attached_file :pdf,   :storage => :s3, :s3_credentials => "#{Rails.root}/config/s3.yml",
                    :path => "pdf/:id/:style/:basename.:extension"
    
    
  
  
  scope :before, lambda {|end_time| {:conditions => ["due_date < ?", Assignment.format_date(end_time)] }}
  scope :after, lambda {|start_time| {:conditions => ["due_date > ?", Assignment.format_date(start_time)] }}

  
  # need to override the json view to return what full_calendar is expecting.
  # http://arshaw.com/fullcalendar/docs/event_data/Event_Object/#FDBDBD #AFE1F8 #FAFAAD
  def as_json(options = {})
    {
      :id => self.id,
      :title => self.name,
      :start => due_date.rfc822,
      :end => due_date.rfc822,
      :recurring => false,
      :url => Rails.application.routes.url_helpers.view_assignment_calender_path(id),
      :color => self.color_code
    }
    
  end
  
  def self.format_date(date_time)
    Time.at(date_time.to_i).to_formatted_s(:db)
  end
  
  def self.search(search)
     if search
      where('name LIKE ?', "%#{search}%")
    else
     scoped
    end
 end
  
end
