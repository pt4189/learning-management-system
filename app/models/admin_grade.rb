class AdminGrade < ActiveRecord::Base
  attr_accessible :grade, :percentage_above
end
