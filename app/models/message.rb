class Message < ActiveRecord::Base

  attr_accessible :body, :is_deleted_by_recipient, :is_deleted_by_sender, :is_read, :recipient, :sender, :subject, :subject_id,:user_tokens, :file_attachment
  
    has_many :users
  
   attr_reader :user_tokens
  
  def user_tokens=(ids)
    self.user_ids = ids.split(",")
  end
  
  has_attached_file :file_attachment,   :storage => :s3, :s3_credentials => "#{Rails.root}/config/s3.yml",
                    :path => "message_attachement/:id/:style/:basename.:extension"
                    
  def self.search(search)
  if search
    find(:all, :conditions => ['subject LIKE ?', "%#{search}%"])
  else
    find(:all)
  end
end

end
