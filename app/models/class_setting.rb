class ClassSetting < ActiveRecord::Base
	
	before_create :set_defaults
	belongs_to :subjects_user
	delegate :user, :subject, to: :subjects_user

	def self.colors
		{Red:"#ffbdbd", Yellow:"#fffdc5", Blue:"#caebfa", Green:"#bae0b8", Orange:"#ffe9c4", Purple:"#dddcff", White:"#ffffff", Violet:"#f9c1fe", Brown:"#f0ead6", Lime:"#dbffbd", Deep_Blue:"#9dc4f5"}
	end

	def self.remainder_for_colection
		["exercise","reading","report","group_project","exam"]
	end

	def available_colors
		ClassSetting.colors.values - user.class_settings.collect(&:color)
	end
	
	def self.remainders
		["Email","None", "On Time", "5 minutes before", "15 minutes before",
		 "30 minutes before", "1 hour before", "2 hours before", "1 day before", "2 days before", "1 week before"]
	end

	def subject_name
		subjects_user.subject.name
	end

	def set_defaults
		self.automatic_remainder = false
		self.color = available_colors.first
		ClassSetting.remainder_for_colection.each do |remainder_for|
			send("#{remainder_for}=", Array.new(11, "false").join(","))
		end
	end

	def update_remainder(remainder_for, remainder, value)
		array = send(remainder_for).split(",")
		array[ClassSetting.remainders.index(remainder)] = value
		update_attribute(remainder_for.to_sym, array.join(","))
	end

	def has_remainder(remainder_for, remainder)
		array = send(remainder_for).split(",")
		array[ClassSetting.remainders.index(remainder)] == "true"
	end

	def update_color(new_color)
		old_color = color
		class_setting_to_change = user.class_settings.compact.select{|c| c.color == new_color}.first
		class_setting_to_change.update_attribute(:color, color) unless class_setting_to_change.nil?
		update_attribute(:color, new_color)
		class_setting_to_change.nil? ? "" : [class_setting_to_change.id, old_color].join(",")
	end
end