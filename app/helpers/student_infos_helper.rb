module StudentInfosHelper
  
  def grade_calculator
    
    total = StudentScore.where(:subject_id=>params[:course_class_id],:user_id=>params[:id]).sum(:assignment_count)
    sumscore = StudentScore.where(:subject_id=>params[:course_class_id],:user_id=>params[:id]).sum(:score)
    cal = (sumscore.to_f/(total.nonzero? || 1))
    grade = cal*100
    
    
    if grade >= 91
       "A+"
    elsif grade >=81 && grade <= 90
       "A"
    elsif grade >=71 && grade <= 80
       "B+"
    elsif grade >=61 && grade <= 70
       "B"
    elsif grade >=51 && grade <= 60
       "C+"
    elsif grade >=41 && grade <= 50
       "C"
    elsif grade >=1 && grade <= 40
       "D"
    else
       "--"
    end
  
  end
  
  
  def class_avg_calculator
    
    total_avg = AverageGrade.where(:subject_id=>params[:course_class_id]).sum(:percentage)
    no_of_student = StudentScore.find_all_by_subject_id(params[:course_class_id]).length
    avg_grade = (total_avg.to_f / (no_of_student.nonzero? || 1))
    
    if avg_grade >= 91
       "A+"
    elsif avg_grade >=81 && avg_grade <= 90
       "A"
    elsif avg_grade >=71 && avg_grade <= 80
       "B+"
    elsif avg_grade >=61 && avg_grade <= 70
       "B"
    elsif avg_grade >=51 && avg_grade <= 60
       "C+"
    elsif avg_grade >=41 && avg_grade <= 50
       "C"
    elsif avg_grade >=1 && avg_grade <= 40
       "D"
    else
       "--"
    end
    
    
  end
  
  
  
  def result_calculator
    
    @grade= StudentAssignment.find_all_by_user_id_and_result_and_assignment_id(params[:id],'1',params[:assignment_id]).length
   
      @std_score = StudentScore.find_by_assignment_id_and_user_id(params[:assignment_id],params[:id])
   
       if @std_score.blank?
         StudentScore.create(:assignment_id => params[:assignment_id], :user_id => params[:id], :score =>@grade,:subject_id =>params[:assignment][:subject_id],:class_id =>params[:assignment][:class_id], :assignment_count =>params[:assignment][:assignment_count])
       else
         StudentScore.update(@std_score.id, :assignment_id => params[:assignment_id], :user_id => params[:id], :score =>@grade,:subject_id =>params[:assignment][:subject_id],:class_id =>params[:assignment][:class_id], :assignment_count =>params[:assignment][:assignment_count])
       end  
   
          @g = ((StudentScore.where(params[:course_class_id],params[:id]).sum(:score).to_f / StudentScore.where(params[:course_class_id],params[:id]).sum(:assignment_count))*100)
         
          @std = AverageGrade.find_by_subject_id_and_user_id(params[:assignment][:subject_id],params[:id])
   
      if @std.blank?
         AverageGrade.create(:class_id =>params[:assignment][:class_id],:subject_id =>params[:assignment][:subject_id],:user_id => params[:id],:percentage =>@g)
      else
         AverageGrade.update(@std.id, :class_id => params[:assignment][:class_id],:subject_id => params[:assignment][:subject_id],:user_id => params[:id],:percentage =>@g)  
      end
   
  end
  
 
  	    
end
