module AdminPeopleHelper
	
	def admin_people_add_path(type)
		case type
		when 's'
			"/users/#{current_user.id}/student/add"
		when 't'
			"/users/#{current_user.id}/teacher/add_teacher"
		when 'i'
			"/users/#{current_user.id}/institute/add_institute"
		end
	end

	def admin_people_edit_path(type, id)
		case type
		when 's'
			edit_student_path(current_user.id, id)
		when 't'
			edit_teacher_path(current_user.id, id)
		when 'i'
			edit_institute_path(current_user.id, id)
		end
	end
end