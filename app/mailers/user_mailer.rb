class UserMailer < ActionMailer::Base
  include SendGrid 
  default from: "course_cloud@gmail.com"

  # Subject can be set in your I18n file at config/locales/en.yml
  # with the following lookup:
  #
  #   en.user_mailer.password_reset.subject
  #
 def password_reset(user)
    @user = user
    mail :to => user.email, :subject => "Password Reset"
 end
 
 def event_mailer(event_user, eve)
   @event_user = event_user
   @eve = eve
   event = event_user.map(&:email)
   mail :to => sendgrid_recipients(event), :subject => "Event Notification from LMS."
 end
def message_mailer(message_user, message)
   @message_user = message_user
   @message = message
   message1 = message_user.email
   mail :to => message1, :subject => "Message Notification from LMS."
 end

end
