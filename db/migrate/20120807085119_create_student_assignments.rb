class CreateStudentAssignments < ActiveRecord::Migration
  def change
    create_table :student_assignments do |t|
      t.integer :question_id
      t.integer :assignment_id
      t.integer :user_id
      t.integer :option_answer
      t.text :explaination
      t.boolean :result

      t.timestamps
    end
  end
end
