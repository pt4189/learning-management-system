class CreateSubobjquestions < ActiveRecord::Migration
  def change
    create_table :subobjquestions do |t|
      t.integer :sub_question_id
      t.string :que

      t.timestamps
    end
  end
end
