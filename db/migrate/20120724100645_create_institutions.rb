class CreateInstitutions < ActiveRecord::Migration
  def change
    create_table :institutions do |t|
      t.string :Name
      t.string :Address
      t.string :Email
      t.integer :user_id
      t.timestamps
    end
  end
end
