class CreateMessages < ActiveRecord::Migration
  def change
    create_table :messages do |t|
      t.integer :sender
      t.integer :recipient
      t.string :subject
      t.text :body
      t.integer :subject_id
      t.boolean  :is_read, :default=>false
      t.boolean  :is_deleted_by_sender, :default=>false
      t.boolean  :is_deleted_by_recipient, :default=>false
      
      t.timestamps
    end
  end
end
