class AddDetails2ToUsers < ActiveRecord::Migration
  def change
    add_column :users, :username, :string
    add_column :users, :class_status, :boolean
  end
end
