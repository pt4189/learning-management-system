class CreateSubjects < ActiveRecord::Migration
  def change
    create_table :subjects do |t|
      t.string  :name
      t.integer :user_id
       t.boolean :status
      t.timestamps
    end
  end
end
