class ReorgSubjects3 < ActiveRecord::Migration
  def change
    add_column :subjects, :description, :string
    add_column :subjects, :code, :string
    add_column :subjects, :seats, :integer
    add_column :subjects, :credits, :decimal, :precision => 4, :scale => 2
    add_column :subjects, :syllabus, :string
  end
end