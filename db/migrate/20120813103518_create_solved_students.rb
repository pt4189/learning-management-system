class CreateSolvedStudents < ActiveRecord::Migration
  def change
    create_table :solved_students do |t|
      t.integer :assignment_id
      t.integer :user_id
      t.timestamps
    end
  end
end
