class AddDetailToSubQuestions < ActiveRecord::Migration
  def change
    add_column :sub_questions, :class_id, :integer
    add_column :sub_questions, :assignment_id, :integer
    
    add_column :sub_questions, :user_id, :integer
  end
end
