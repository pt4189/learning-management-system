class AddColorToSubjects < ActiveRecord::Migration
  def change
    add_column :subjects, :color_code, :string
  end
end
