class AddAttachmentPdfToAssignments < ActiveRecord::Migration
  def self.up
    change_table :assignments do |t|
      t.has_attached_file :pdf
    end
  end

  def self.down
    drop_attached_file :assignments, :pdf
  end
end
