class CreateStaffs < ActiveRecord::Migration
  def change
    create_table :staffs do |t|
      t.integer :class_id
      t.integer :user_id
      t.boolean :status

      t.timestamps
    end
  end
end
