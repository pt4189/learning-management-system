class AddTimeToEduClasses < ActiveRecord::Migration
  def change
    add_column :edu_classes, :start_time, :time
    add_column :edu_classes, :end_time, :time
  end
end
