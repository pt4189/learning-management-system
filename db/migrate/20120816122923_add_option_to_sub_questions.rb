class AddOptionToSubQuestions < ActiveRecord::Migration
  def change
    add_column :sub_questions, :option_answer, :integer
  end
end
