class CreateUnits < ActiveRecord::Migration
  def change
    create_table :units do |t|
      t.string :name 
      t.integer :subject_id
       t.boolean :status
      t.timestamps
    end
  end
end
