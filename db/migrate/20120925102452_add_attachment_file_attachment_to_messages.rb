class AddAttachmentFileAttachmentToMessages < ActiveRecord::Migration
  def self.up
    change_table :messages do |t|
      t.has_attached_file :file_attachment
    end
  end

  def self.down
    drop_attached_file :messages, :file_attachment
  end
end
