class CreateSubQuestions < ActiveRecord::Migration
  def change
    create_table :sub_questions do |t|
      t.string :subquestion
      t.integer :type_of_question
      t.boolean :option
      t.string  :explaination
      t.string  :correct_answer 
       t.integer :question_id
       t.boolean :status
      t.timestamps
    end
  end
end
