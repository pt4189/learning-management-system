class AddAskMeToUsers < ActiveRecord::Migration
  def change
    add_column :users, :dont_ask, :boolean, :default=>false
  end
end
