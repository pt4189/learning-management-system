class CreateAssignments < ActiveRecord::Migration
  def change
    create_table :assignments do |t|
      t.string :name
      t.string :assignment_type
      t.datetime :due_date
      t.integer :class_id
      t.integer :subject_id
      t.integer :unit_id
      t.integer :chapter_id
      t.integer :topic_id
      t.integer :sub_topic_id
      t.integer :max_point
      t.integer :user_id

      t.timestamps
    end
  end
end
