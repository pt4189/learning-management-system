class AddTermToEduClasses < ActiveRecord::Migration
  def change
    add_column :edu_classes, :terms, :string
    add_column :edu_classes, :year, :integer
    add_column :edu_classes, :sem, :string
  end
end
