class AddGraduationToEduClass < ActiveRecord::Migration
  def change
    add_column :edu_classes, :graduation_month, :integer
    add_column :edu_classes, :graduation_year, :integer
  end
end
