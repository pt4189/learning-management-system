class CreateDaysSubjects < ActiveRecord::Migration
	def change
		create_table :days_subjects do |t|
			t.integer "day_id"
			t.integer "subject_id"
			t.timestamps
		end
	end
end
