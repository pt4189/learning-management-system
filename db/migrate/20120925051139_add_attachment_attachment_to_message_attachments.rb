class AddAttachmentAttachmentToMessageAttachments < ActiveRecord::Migration
  def self.up
    change_table :message_attachments do |t|
      t.has_attached_file :attachment
    end
  end

  def self.down
    drop_attached_file :message_attachments, :attachment
  end
end
