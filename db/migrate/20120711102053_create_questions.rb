class CreateQuestions < ActiveRecord::Migration
  def change
    create_table :questions do |t|
      t.string :question
      t.integer :type_of_question
      t.boolean :option
      t.string  :explaination
      t.string  :correct_answer
      t.integer :topic_id
      t.integer :sub_topic_id
       t.integer :assignment_id
       t.boolean :status
      t.timestamps
    end
  end
end
