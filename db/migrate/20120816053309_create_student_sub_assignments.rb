class CreateStudentSubAssignments < ActiveRecord::Migration
  def change
    create_table :student_sub_assignments do |t|
      t.integer :assignment_id
      t.integer :question_id
      t.integer :sub_question_id
      t.integer :user_id
      t.integer :option_answer
      t.text :explaination
      t.boolean :result

      t.timestamps
    end
  end
end
