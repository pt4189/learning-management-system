class AddClassIdToSubjects < ActiveRecord::Migration
  def change
    add_column :subjects, :class_id, :integer
  end
end
