class CreateObjquestions < ActiveRecord::Migration
  def change
    create_table :objquestions do |t|
      t.integer :question_id
      t.string :que

      t.timestamps
    end
  end
end
