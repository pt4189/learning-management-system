class CreateClassSettings < ActiveRecord::Migration
  def change
  	create_table :class_settings do |t|
      t.integer :staff_id
      t.string :color
      t.boolean :automatic_remainder
      t.string :exercise
      t.string :reading
      t.string :report
      t.string :group_project
      t.string :exam
      t.timestamps
    end
  end
end
