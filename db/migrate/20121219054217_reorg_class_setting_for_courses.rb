class ReorgClassSettingForCourses < ActiveRecord::Migration
  def up
  	drop_table :subjects_users

  	create_table "subjects_users", :force => true do |t|
    	t.integer "subject_id"
    	t.integer "user_id"
  	end

  	rename_column :class_settings, :staff_id, :subjects_user_id
  	remove_column :class_settings, :entity_type
  end

  def down
  	drop_table :subjects_users

  	create_table "subjects_users", :id => false, :force => true do |t|
    	t.integer "subject_id"
    	t.integer "user_id"
  	end

  	rename_column :class_settings, :subjects_user_id, :staff_id
  	add_column :class_settings, :entity_type, :string
  end
end