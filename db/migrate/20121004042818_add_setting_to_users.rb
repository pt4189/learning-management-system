class AddSettingToUsers < ActiveRecord::Migration
  def change
    add_column :users, :setting, :boolean, :default=>true
  end
end
