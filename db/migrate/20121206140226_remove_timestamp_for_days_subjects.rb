class RemoveTimestampForDaysSubjects < ActiveRecord::Migration
  def up
  	remove_column :days_subjects, :created_at
  	remove_column :days_subjects, :updated_at
  end

  def down
  	add_column :days_subjects, :created_at, :datetime
  	add_column :days_subjects, :updated_at, :datetime
  end
end
