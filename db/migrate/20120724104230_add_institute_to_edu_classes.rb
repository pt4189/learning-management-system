class AddInstituteToEduClasses < ActiveRecord::Migration
  def change
    add_column :edu_classes, :institute_id, :integer
  end
end
