class AddInstituteToChapters < ActiveRecord::Migration
  def change
    add_column :chapters, :institute_id, :integer
  end
end
