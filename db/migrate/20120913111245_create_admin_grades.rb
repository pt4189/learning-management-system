class CreateAdminGrades < ActiveRecord::Migration
  def change
    create_table :admin_grades do |t|
      t.string :grade
      t.integer :percentage_above

      t.timestamps
    end
  end
end
