class CreateStudentRanks < ActiveRecord::Migration
  def change
    create_table :student_ranks do |t|
      t.integer :class_id
      t.integer :user_id
      t.integer :percentage
      t.timestamps
    end
  end
end
