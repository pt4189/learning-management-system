class CreateStudentScores < ActiveRecord::Migration
  def change
    create_table :student_scores do |t|
      t.integer :assignment_id
      t.integer :user_id
      t.integer :score
      t.integer :subject_id
      t.integer :class_id
      t.integer :assignment_count
      t.timestamps
    end
  end
end
