class ReorgSubjectsTable < ActiveRecord::Migration
  def up
  	remove_column :subjects, :user_id
  	remove_column :subjects, :class_id
  	remove_column :subjects, :color_code
  	remove_column :subjects, :color_class

    add_column :subjects, :subject_days, :integer
    add_column :subjects, :subject_hours, :integer
    add_column :subjects, :terms, :string
    add_column :subjects, :subject_location, :string
    add_column :subjects, :start_time, :time
    add_column :subjects, :end_time, :time

  end

  def down
  	add_column :subjects, :user_id, :integer
  	add_column :subjects, :class_id, :integer
  	add_column :subjects, :color_code, :string
  	add_column :subjects, :color_class, :string

    remove_column :subjects, :subject_days
    remove_column :subjects, :subject_hours
    remove_column :subjects, :terms
    remove_column :subjects, :subject_location
    remove_column :subjects, :start_time
    remove_column :subjects, :end_time
  end
end
