class CreateSolvePdfAssignments < ActiveRecord::Migration
  def change
    create_table :solve_pdf_assignments do |t|
      t.integer :class_id
      t.integer :subject_id
      t.integer :assignment_id
      t.integer :user_id
      t.timestamps
    end
  end
end
