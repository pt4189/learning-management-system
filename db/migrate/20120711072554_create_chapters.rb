class CreateChapters < ActiveRecord::Migration
  def change
    create_table :chapters do |t|
      t.string :name
      t.integer :unit_id
       t.boolean :status
      t.timestamps
    end
  end
end
