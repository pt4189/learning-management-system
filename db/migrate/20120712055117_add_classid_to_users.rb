class AddClassidToUsers < ActiveRecord::Migration
  def self.up
    add_column :users, :class_id, :integer
    add_column :users, :designation,:string
  end

  def self.down
   remove_column :users,:class_id
   remove_column :users,:designation
  end
end
