class AddCountToAssignments < ActiveRecord::Migration
def self.up
    add_column :assignments, :questions_count, :integer,:default => 0
  end

  def self.down
    remove :assignments
  end 
end

