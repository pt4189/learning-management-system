class CreateEduClasses < ActiveRecord::Migration
  def change
    create_table :edu_classes do |t|
      t.string  :class_name
      t.integer :class_days
      t.integer :class_hours
      t.string  :class_location
      t.integer :no_of_seats
      t.integer :user_id
      t.boolean :status, :null => false, :default => true
      t.timestamps
    end
  end
end
