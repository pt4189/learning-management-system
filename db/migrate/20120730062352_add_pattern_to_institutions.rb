class AddPatternToInstitutions < ActiveRecord::Migration
  def change
    add_column :institutions, :terms, :string
    add_column :institutions, :year, :integer
    add_column :institutions, :sem, :string
  end
end
