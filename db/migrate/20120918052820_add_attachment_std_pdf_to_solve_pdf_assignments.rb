class AddAttachmentStdPdfToSolvePdfAssignments < ActiveRecord::Migration
  def self.up
    change_table :solve_pdf_assignments do |t|
      t.has_attached_file :std_pdf
    end
  end

  def self.down
    drop_attached_file :solve_pdf_assignments, :std_pdf
  end
end
