class AddIndexToQuestions < ActiveRecord::Migration
  def self.up
    add_index :questions, :assignment_id
  end

  def self.down
    remove :questions
  end

end
