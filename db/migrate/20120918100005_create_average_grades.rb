class CreateAverageGrades < ActiveRecord::Migration
  def change
    create_table :average_grades do |t|
      t.integer :class_id
      t.integer :subject_id
      t.integer :user_id
      t.decimal :percentage 
      t.timestamps
    end
  end
end
