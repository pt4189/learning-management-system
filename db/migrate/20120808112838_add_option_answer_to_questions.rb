class AddOptionAnswerToQuestions < ActiveRecord::Migration
  def change
    add_column :questions, :option_answer, :integer
  end
end
