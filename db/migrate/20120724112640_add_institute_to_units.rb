class AddInstituteToUnits < ActiveRecord::Migration
  def change
    add_column :units, :institute_id, :integer
  end
end
