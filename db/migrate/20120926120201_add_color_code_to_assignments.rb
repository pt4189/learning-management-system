class AddColorCodeToAssignments < ActiveRecord::Migration
  def change
        add_column :assignments, :color_code, :string
  end
end
