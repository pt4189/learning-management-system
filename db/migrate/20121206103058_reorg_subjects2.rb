class ReorgSubjects2 < ActiveRecord::Migration
  def change
    add_column :subjects, :year, :string
    add_column :subjects, :sem, :string
  end
end