class AddColorClassToSubjects < ActiveRecord::Migration
  def change
    add_column :subjects, :color_class, :string
  end
end
