class CreateSubTopics < ActiveRecord::Migration
  def change
    create_table :sub_topics do |t|
      t.string :name
      t.integer :topic_id
      t.boolean :status_id
      t.timestamps
    end
  end
end
