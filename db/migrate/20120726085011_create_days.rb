class CreateDays < ActiveRecord::Migration
  def change
    create_table :days do |t|
      t.string :day
      t.string :abbreviate

      t.timestamps
    end
  end
end
