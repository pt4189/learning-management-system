class DaysHaveAndBelongsToManyEduClasses < ActiveRecord::Migration
  def self.up
      create_table :days_edu_classes, :id => false do |t|
        t.references :day, :edu_class
    end
  end

    def self.down
      drop_table :days_edu_classes
    end
end
