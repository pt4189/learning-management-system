# encoding: UTF-8
# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended to check this file into your version control system.

ActiveRecord::Schema.define(:version => 20130108184227) do

  create_table "admin_grades", :force => true do |t|
    t.string   "grade"
    t.integer  "percentage_above"
    t.datetime "created_at",       :null => false
    t.datetime "updated_at",       :null => false
  end

  create_table "assignments", :force => true do |t|
    t.string   "name"
    t.string   "assignment_type"
    t.datetime "due_date"
    t.integer  "class_id"
    t.integer  "subject_id"
    t.integer  "unit_id"
    t.integer  "chapter_id"
    t.integer  "topic_id"
    t.integer  "sub_topic_id"
    t.integer  "max_point"
    t.integer  "user_id"
    t.datetime "created_at",                      :null => false
    t.datetime "updated_at",                      :null => false
    t.integer  "questions_count",  :default => 0
    t.string   "pdf_file_name"
    t.string   "pdf_content_type"
    t.integer  "pdf_file_size"
    t.datetime "pdf_updated_at"
    t.string   "color_code"
    t.text     "notes"
  end

  create_table "average_grades", :force => true do |t|
    t.integer  "class_id"
    t.integer  "subject_id"
    t.integer  "user_id"
    t.decimal  "percentage", :precision => 10, :scale => 0
    t.datetime "created_at",                                :null => false
    t.datetime "updated_at",                                :null => false
  end

  create_table "chapters", :force => true do |t|
    t.string   "name"
    t.integer  "unit_id"
    t.boolean  "status"
    t.datetime "created_at",   :null => false
    t.datetime "updated_at",   :null => false
    t.integer  "institute_id"
  end

  create_table "class_settings", :force => true do |t|
    t.integer  "subjects_user_id"
    t.string   "color"
    t.boolean  "automatic_remainder"
    t.string   "exercise"
    t.string   "reading"
    t.string   "report"
    t.string   "group_project"
    t.string   "exam"
    t.datetime "created_at",          :null => false
    t.datetime "updated_at",          :null => false
  end

  create_table "colors", :force => true do |t|
    t.string   "color"
    t.integer  "color_code"
    t.datetime "created_at", :null => false
    t.datetime "updated_at", :null => false
  end

  create_table "days", :force => true do |t|
    t.string   "day"
    t.string   "abbreviate"
    t.datetime "created_at", :null => false
    t.datetime "updated_at", :null => false
  end

  create_table "days_edu_classes", :id => false, :force => true do |t|
    t.integer "day_id"
    t.integer "edu_class_id"
  end

  create_table "days_subjects", :force => true do |t|
    t.integer "day_id"
    t.integer "subject_id"
  end

  create_table "edu_classes", :force => true do |t|
    t.string   "class_name"
    t.integer  "class_days"
    t.integer  "class_hours"
    t.string   "class_location"
    t.integer  "no_of_seats"
    t.integer  "user_id"
    t.boolean  "status",           :default => true, :null => false
    t.datetime "created_at",                         :null => false
    t.datetime "updated_at",                         :null => false
    t.integer  "institute_id"
    t.time     "start_time"
    t.time     "end_time"
    t.string   "terms"
    t.integer  "year"
    t.string   "sem"
    t.integer  "graduation_month"
    t.integer  "graduation_year"
  end

  create_table "events", :force => true do |t|
    t.string   "name"
    t.text     "location"
    t.datetime "date"
    t.text     "notes"
    t.datetime "created_at", :null => false
    t.datetime "updated_at", :null => false
    t.datetime "start_date"
    t.datetime "end_date"
    t.string   "repeat"
  end

  create_table "forem_categories", :force => true do |t|
    t.string   "name",       :null => false
    t.datetime "created_at", :null => false
    t.datetime "updated_at", :null => false
    t.string   "slug"
  end

  add_index "forem_categories", ["slug"], :name => "index_forem_categories_on_slug", :unique => true

  create_table "forem_forums", :force => true do |t|
    t.string  "name"
    t.text    "description"
    t.integer "category_id"
    t.integer "views_count", :default => 0
    t.string  "slug"
    t.integer "for_id"
  end

  add_index "forem_forums", ["slug"], :name => "index_forem_forums_on_slug", :unique => true

  create_table "forem_groups", :force => true do |t|
    t.string "name"
  end

  add_index "forem_groups", ["name"], :name => "index_forem_groups_on_name"

  create_table "forem_memberships", :force => true do |t|
    t.integer "group_id"
    t.integer "member_id"
  end

  add_index "forem_memberships", ["group_id"], :name => "index_forem_memberships_on_group_id"

  create_table "forem_moderator_groups", :force => true do |t|
    t.integer "forum_id"
    t.integer "group_id"
  end

  add_index "forem_moderator_groups", ["forum_id"], :name => "index_forem_moderator_groups_on_forum_id"

  create_table "forem_posts", :force => true do |t|
    t.integer  "topic_id"
    t.text     "text"
    t.integer  "user_id"
    t.datetime "created_at",                                :null => false
    t.datetime "updated_at",                                :null => false
    t.integer  "reply_to_id"
    t.string   "state",       :default => "pending_review"
    t.boolean  "notified",    :default => false
  end

  add_index "forem_posts", ["reply_to_id"], :name => "index_forem_posts_on_reply_to_id"
  add_index "forem_posts", ["state"], :name => "index_forem_posts_on_state"
  add_index "forem_posts", ["topic_id"], :name => "index_forem_posts_on_topic_id"
  add_index "forem_posts", ["user_id"], :name => "index_forem_posts_on_user_id"

  create_table "forem_subscriptions", :force => true do |t|
    t.integer "subscriber_id"
    t.integer "topic_id"
  end

  create_table "forem_topics", :force => true do |t|
    t.integer  "forum_id"
    t.integer  "user_id"
    t.string   "subject"
    t.datetime "created_at",                                 :null => false
    t.datetime "updated_at",                                 :null => false
    t.boolean  "locked",       :default => false,            :null => false
    t.boolean  "pinned",       :default => false
    t.boolean  "hidden",       :default => false
    t.datetime "last_post_at"
    t.string   "state",        :default => "pending_review"
    t.integer  "views_count",  :default => 0
    t.string   "slug"
  end

  add_index "forem_topics", ["forum_id"], :name => "index_forem_topics_on_forum_id"
  add_index "forem_topics", ["slug"], :name => "index_forem_topics_on_slug", :unique => true
  add_index "forem_topics", ["state"], :name => "index_forem_topics_on_state"
  add_index "forem_topics", ["user_id"], :name => "index_forem_topics_on_user_id"

  create_table "forem_views", :force => true do |t|
    t.integer  "user_id"
    t.integer  "viewable_id"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.integer  "count",             :default => 0
    t.string   "viewable_type"
    t.datetime "current_viewed_at"
    t.datetime "past_viewed_at"
  end

  add_index "forem_views", ["updated_at"], :name => "index_forem_views_on_updated_at"
  add_index "forem_views", ["user_id"], :name => "index_forem_views_on_user_id"
  add_index "forem_views", ["viewable_id"], :name => "index_forem_views_on_topic_id"

  create_table "institutions", :force => true do |t|
    t.string   "Name"
    t.string   "Address"
    t.string   "Email"
    t.integer  "user_id"
    t.datetime "created_at", :null => false
    t.datetime "updated_at", :null => false
    t.string   "terms"
    t.integer  "year"
    t.string   "sem"
  end

  create_table "message_attachments", :force => true do |t|
    t.integer  "message_id"
    t.datetime "created_at",              :null => false
    t.datetime "updated_at",              :null => false
    t.string   "attachment_file_name"
    t.string   "attachment_content_type"
    t.integer  "attachment_file_size"
    t.datetime "attachment_updated_at"
  end

  create_table "messages", :force => true do |t|
    t.integer  "sender"
    t.integer  "recipient"
    t.string   "subject"
    t.text     "body"
    t.integer  "subject_id"
    t.boolean  "is_read",                      :default => false
    t.boolean  "is_deleted_by_sender",         :default => false
    t.boolean  "is_deleted_by_recipient",      :default => false
    t.datetime "created_at",                                      :null => false
    t.datetime "updated_at",                                      :null => false
    t.string   "file_attachment_file_name"
    t.string   "file_attachment_content_type"
    t.integer  "file_attachment_file_size"
    t.datetime "file_attachment_updated_at"
  end

  create_table "objquestions", :force => true do |t|
    t.integer  "question_id"
    t.string   "que"
    t.datetime "created_at",  :null => false
    t.datetime "updated_at",  :null => false
  end

  create_table "questions", :force => true do |t|
    t.string   "question"
    t.integer  "type_of_question"
    t.boolean  "option"
    t.string   "explaination"
    t.string   "correct_answer"
    t.integer  "topic_id"
    t.integer  "sub_topic_id"
    t.integer  "assignment_id"
    t.boolean  "status"
    t.datetime "created_at",       :null => false
    t.datetime "updated_at",       :null => false
    t.integer  "user_id"
    t.integer  "option_answer"
  end

  add_index "questions", ["assignment_id"], :name => "index_questions_on_assignment_id"

  create_table "roles", :force => true do |t|
    t.string   "name"
    t.datetime "created_at", :null => false
    t.datetime "updated_at", :null => false
  end

  create_table "roles_users", :id => false, :force => true do |t|
    t.integer "role_id"
    t.integer "user_id"
  end

  create_table "solve_pdf_assignments", :force => true do |t|
    t.integer  "class_id"
    t.integer  "subject_id"
    t.integer  "assignment_id"
    t.integer  "user_id"
    t.datetime "created_at",           :null => false
    t.datetime "updated_at",           :null => false
    t.string   "std_pdf_file_name"
    t.string   "std_pdf_content_type"
    t.integer  "std_pdf_file_size"
    t.datetime "std_pdf_updated_at"
  end

  create_table "solved_students", :force => true do |t|
    t.integer  "assignment_id"
    t.integer  "user_id"
    t.datetime "created_at",    :null => false
    t.datetime "updated_at",    :null => false
  end

  create_table "staffs", :force => true do |t|
    t.integer  "class_id"
    t.integer  "user_id"
    t.boolean  "status"
    t.datetime "created_at",   :null => false
    t.datetime "updated_at",   :null => false
    t.integer  "institute_id"
  end

  create_table "student_assignments", :force => true do |t|
    t.integer  "question_id"
    t.integer  "assignment_id"
    t.integer  "user_id"
    t.integer  "option_answer"
    t.text     "explaination"
    t.boolean  "result"
    t.datetime "created_at",    :null => false
    t.datetime "updated_at",    :null => false
  end

  create_table "student_ranks", :force => true do |t|
    t.integer  "class_id"
    t.integer  "user_id"
    t.integer  "percentage"
    t.datetime "created_at", :null => false
    t.datetime "updated_at", :null => false
  end

  create_table "student_scores", :force => true do |t|
    t.integer  "assignment_id"
    t.integer  "user_id"
    t.integer  "score"
    t.integer  "subject_id"
    t.integer  "class_id"
    t.integer  "assignment_count"
    t.datetime "created_at",       :null => false
    t.datetime "updated_at",       :null => false
  end

  create_table "student_sub_assignments", :force => true do |t|
    t.integer  "assignment_id"
    t.integer  "question_id"
    t.integer  "sub_question_id"
    t.integer  "user_id"
    t.integer  "option_answer"
    t.text     "explaination"
    t.boolean  "result"
    t.datetime "created_at",      :null => false
    t.datetime "updated_at",      :null => false
  end

  create_table "sub_questions", :force => true do |t|
    t.string   "subquestion"
    t.integer  "type_of_question"
    t.boolean  "option"
    t.string   "explaination"
    t.string   "correct_answer"
    t.integer  "question_id"
    t.boolean  "status"
    t.datetime "created_at",       :null => false
    t.datetime "updated_at",       :null => false
    t.integer  "class_id"
    t.integer  "assignment_id"
    t.integer  "user_id"
    t.integer  "option_answer"
  end

  create_table "sub_topics", :force => true do |t|
    t.string   "name"
    t.integer  "topic_id"
    t.boolean  "status_id"
    t.datetime "created_at",   :null => false
    t.datetime "updated_at",   :null => false
    t.integer  "institute_id"
  end

  create_table "subjects", :force => true do |t|
    t.string   "name"
    t.boolean  "status"
    t.datetime "created_at",                                     :null => false
    t.datetime "updated_at",                                     :null => false
    t.integer  "institute_id"
    t.integer  "subject_days"
    t.integer  "subject_hours"
    t.string   "terms"
    t.string   "subject_location"
    t.time     "start_time"
    t.time     "end_time"
    t.string   "year"
    t.string   "sem"
    t.string   "description"
    t.string   "code"
    t.integer  "seats"
    t.decimal  "credits",          :precision => 4, :scale => 2
    t.string   "syllabus"
  end

  create_table "subjects_users", :force => true do |t|
    t.integer "subject_id"
    t.integer "user_id"
  end

  create_table "subobjquestions", :force => true do |t|
    t.integer  "sub_question_id"
    t.string   "que"
    t.datetime "created_at",      :null => false
    t.datetime "updated_at",      :null => false
  end

  create_table "topics", :force => true do |t|
    t.string   "name"
    t.integer  "chapter_id"
    t.boolean  "status_id"
    t.datetime "created_at",   :null => false
    t.datetime "updated_at",   :null => false
    t.integer  "institute_id"
  end

  create_table "units", :force => true do |t|
    t.string   "name"
    t.integer  "subject_id"
    t.boolean  "status"
    t.datetime "created_at",   :null => false
    t.datetime "updated_at",   :null => false
    t.integer  "institute_id"
  end

  create_table "users", :force => true do |t|
    t.string   "name"
    t.string   "email"
    t.datetime "created_at",                                           :null => false
    t.datetime "updated_at",                                           :null => false
    t.string   "encrypted_password"
    t.string   "salt"
    t.string   "auth_token"
    t.string   "password_reset_token"
    t.datetime "password_reset_sent_at"
    t.integer  "class_id"
    t.string   "designation"
    t.string   "username"
    t.boolean  "class_status"
    t.string   "first_name"
    t.string   "last_name"
    t.integer  "institute_id"
    t.string   "photo_file_name"
    t.string   "photo_content_type"
    t.integer  "photo_file_size"
    t.datetime "photo_updated_at"
    t.boolean  "dont_ask",               :default => false
    t.boolean  "setting",                :default => true
    t.string   "language"
    t.boolean  "forem_admin",            :default => false
    t.string   "forem_state",            :default => "pending_review"
    t.boolean  "forem_auto_subscribe",   :default => false
  end

  create_table "videos", :force => true do |t|
    t.string   "title"
    t.string   "panda_video_id"
    t.integer  "class_id"
    t.integer  "subject_id"
    t.integer  "user_id"
    t.datetime "created_at",     :null => false
    t.datetime "updated_at",     :null => false
  end

end
