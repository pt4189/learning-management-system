# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rake db:seed (or created alongside the db with db:setup).
#
# Examples:
#
#   cities = City.create([{ name: 'Chicago' }, { name: 'Copenhagen' }])
#   Mayor.create(name: 'Emanuel', city: cities.first)


roles = Role.create([{:name=> 'admin'}, {:name=> 'teacher'}, {:name=> 'student'}])
p "3 Roles Defined in the system - Admin, Teacher, Student"

admin = User.create({:username => 'admin', :email=> 'admin@admin.com', :password => 'admin123!', :first_name => "admin", 
	:last_name => "admin"})
admin.roles = [Role.first]
admin.save
p "Admin created with username: admin, pwd: admin123!"

Institution.create([{Name: "MIT", Email: "mit@mit.com"}, {Name: "PSG", Email: "PSG@PSG.in"}])
p "2 Institution created - MIT, PSG"

teacher = User.create({:username => 'macd', :email=> 'macd@macd.com', :password => '123456', :first_name => "mac", 
	:last_name => "donald", designation: "t"})
teacher.roles = [Role.all[1]]
teacher.save
p "Teacher created with username: macd, pwd:123456"

student = User.create({:username => 'student', :email=> 'student@student.com', :password => '123456', :first_name => "student", 
	:last_name => "last name", designation: "s"})
student.roles = [Role.all[2]]
student.save
p "Student created with username: student, pwd:123456"

Day.create([{day: "Monday", abbreviate: "M"}, {day: "Tuesday", abbreviate: "T"}, {day: "Wednessday", abbreviate: "W"}, {day: "Thursday", abbreviate: "Th"}, {day: "Friday", abbreviate: "F"}, {day: "Saturday", abbreviate: "Sat"}, {day: "Sunday", abbreviate: "Sun"}])
p "7 Day created in DB"

Forem::Category.create({name: "LMSGENERAL"})