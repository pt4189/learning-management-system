require 'test_helper'

class AdminGradesControllerTest < ActionController::TestCase
  setup do
    @admin_grade = admin_grades(:one)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:admin_grades)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create admin_grade" do
    assert_difference('AdminGrade.count') do
      post :create, admin_grade: { grade: @admin_grade.grade, percentage_above: @admin_grade.percentage_above }
    end

    assert_redirected_to admin_grade_path(assigns(:admin_grade))
  end

  test "should show admin_grade" do
    get :show, id: @admin_grade
    assert_response :success
  end

  test "should get edit" do
    get :edit, id: @admin_grade
    assert_response :success
  end

  test "should update admin_grade" do
    put :update, id: @admin_grade, admin_grade: { grade: @admin_grade.grade, percentage_above: @admin_grade.percentage_above }
    assert_redirected_to admin_grade_path(assigns(:admin_grade))
  end

  test "should destroy admin_grade" do
    assert_difference('AdminGrade.count', -1) do
      delete :destroy, id: @admin_grade
    end

    assert_redirected_to admin_grades_path
  end
end
