# Place all the behaviors and hooks related to the matching controller here.
# All this logic will automatically be available in application.js.
# You can use CoffeeScript in this file: http://jashkenas.github.com/coffee-script/
jQuery(document).ready ->  
  jQuery('#calendar').fullCalendar
    editable: true,        
    header:
      left: 'colors',
      center: 'prev,title,next',
      right: 'list,agendaWeek,agendaDay,month'
    defaultView: 'month',
    height: 500,
    slotMinutes: 15,
      
    eventSources: [{
      url: '/events',
      ignoreTimezone: false
    },{
      url: '/calender_asses',
      ignoreTimezone: false
    }], 
      
    timeFormat: 'h:mm t{ - h:mm t} ',
    dragOpacity: "0.5"
  
    eventDrop: (event, dayDelta, minuteDelta, allDay, revertFunc) ->
      updateEvent(event);

    eventResize: (event, dayDelta, minuteDelta, revertFunc) ->
      updateEvent(event);

    

      
