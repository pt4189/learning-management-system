﻿/***************************/
//@Author: Adrian "yEnS" Mato Gondelle & Ivan Guardado Castro
//@website: www.yensdesign.com
//@email: yensamg@gmail.com
//@license: Feel free to use it, but keep this credits please!					
/***************************/

jQuery(document).ready(function(){
	jQuery(".menu > li").click(function(e){
		switch(e.target.id){
			case "all":
				//change status & style menu
				jQuery("#all").addClass("active");
				jQuery("#date").removeClass("active");
				jQuery("#type").removeClass("active");
				jQuery("#four").removeClass("active");
				//display selected division, hide others
				jQuery("div.all").fadeIn();
				jQuery("div.date").css("display", "none");
				jQuery("div.type").css("display", "none");
				jQuery("div.four").css("display", "none");
			break;
			case "date":
				//change status & style menu
				jQuery("#all").removeClass("active");
				jQuery("#date").addClass("active");
				jQuery("#type").removeClass("active");
				jQuery("#four").removeClass("active");
				//display selected division, hide others
				jQuery("div.date").fadeIn();
				jQuery("div.all").css("display", "none");
				jQuery("div.type").css("display", "none");
				jQuery("div.four").css("display", "none");
				
				break;
			case "four":
				//change status & style menu
				jQuery("#all").removeClass("active");
				jQuery("#date").removeClass("active");
				jQuery("#type").removeClass("active");
				jQuery("#four").addClass("active");
				//display selected division, hide others
				jQuery("div.four").fadeIn();
				jQuery("div.all").css("display", "none");
				jQuery("div.type").css("display", "none");
				jQuery("div.date").css("display", "none");
			break;
			case "type":
				//change status & style menu
				jQuery("#all").removeClass("active");
				jQuery("#date").removeClass("active");
				jQuery("#type").addClass("active");
				jQuery("#four").removeClass("active");
				//display selected division, hide others
				jQuery("div.type").fadeIn();
				jQuery("div.all").css("display", "none");
				jQuery("div.date").css("display", "none");
				jQuery("div.four").css("display", "none");
			break;
		}
		//alert(e.target.id);
		return false;
	});
});